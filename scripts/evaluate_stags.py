import sys
from collections import defaultdict

def compare_tags(gold, predict):

    correct_at_n = defaultdict(int)
    assert len(gold)==len(predict)

    for i in range(len(gold)):
        goldtag = gold[i]
        predicttags = predict[i]
        for i in range(len(predicttags)):
            if goldtag in predicttags[:i+1]:
                correct_at_n[i+1] += 1
       
    acc_at_n = {}
    for key in correct_at_n:
        acc_at_n[key] = correct_at_n[key]/float(len(gold))
 
    return correct_at_n, acc_at_n 
        


def get_nbest_tags(predict_line):
    tokens = predict_line.split(" ")
    result = []
    for t in tokens:
        parts = t.split("/")
        nbest = [(x.split(":"))[0] for x in parts]
        result.append(nbest)
    return result


with open(sys.argv[1]) as gold_file, open(sys.argv[2]) as predict_file: 
    
    total_correct = defaultdict(int) 
    total_length = 0
    acc_sum = defaultdict(float)

    i=0
    for gold_line in gold_file: 
        predict_line = predict_file.next()
        predict_tags = get_nbest_tags(predict_line)
        gold_tags = gold_line.strip().split()
        print gold_tags
        print predict_tags
        correct_at_n, acc_at_n = compare_tags(gold_tags, predict_tags)

        for k in correct_at_n: 
            total_correct[k] += correct_at_n[k]

        for k in acc_at_n: 
            acc_sum[k] += acc_at_n[k]

        #total_correct += correct
        #acc_sum += acc
        i+=1
        total_length += len(gold_tags)
        #print correct, "/", len(gold_tags), "=", acc 
        
    print "==="
    print "Macro avg:"
    for k in acc_sum: 
        print k,"best:" , acc_sum[k] / float(i)
 
    print "Micro avg:"
    for k in total_correct:
        print k,"best:", total_correct[k] / float(total_length)

