package edu.columbia.nlp.gbdp.perceptron_model;

import edu.columbia.nlp.gbdp.Chart;
import edu.columbia.nlp.gbdp.Edge;
import edu.columbia.nlp.gbdp.Traversal;
import edu.columbia.nlp.gbdp.grammar.NonterminalRuleToken;
import edu.columbia.nlp.gbdp.taggers.Token;
import edu.columbia.nlp.util.averaged_perceptron.DataInstance;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by bauer on 10/4/16.
 */
public class TraversalWithContext implements DataInstance {


  private Traversal traversal;
  private Chart chart;
  private List<Token[]> input;

  public TraversalWithContext(Traversal t, Chart c, List<Token[]> inp) {
    traversal = t;
    chart = c;
    input = inp;
  }

  public Traversal getTraversal() {
    return traversal;
  }

  public Chart getChart() {
    return chart;
  }

  public List<Token[]> getInput() {
    return input;
  }

  @Override
  public String getLabel() {
    return null;
  }

  @Override
  public String getDescription() {
    return null;
  }

  /**
   * @return the number of adjunctions at the same site that led to the active item
   * in this traversal under the best current analysis on the chart, or -1 if this
   * traversal is not an adjunction operation.
   */
  public Integer getAdjunctionCount() {
    Edge active = traversal.getActive();
    Edge passive = traversal.getPassive();

    if (active == null) // predict
      return null;
    if (passive == null) // null adjunction
      return null;
    if (!((NonterminalRuleToken) active.getOutside()).hasKleene) // substitution
      return null;
    // Now we know this is an adjunction

    int count = 0;
    Traversal t;
    Edge current = active;
    boolean hasNext = true;
    while (hasNext) {
      count += 1;
      t = chart.last_backpointer.get(current);
      if (t== null) {
        hasNext = false;
      } else {
        current = t.getActive();
        if (current.getDot() != active.getDot())
          hasNext = false;
      }
    }
    return count;
  }

  public List<Integer> getLeftSiblingRules() {
    Edge active = traversal.getActive();
    Edge passive = traversal.getPassive();

    List<Integer> result = new ArrayList<>();

    if (active == null)
      return null;

    boolean hasNext = true;

    Traversal t;
    t = chart.last_backpointer.get(active);
    if (t == null) {
      hasNext = false;
    } else {
      active = t.getActive();
      passive = t.getPassive();
    }

    while (hasNext) {
      if (passive != null)
        result.add(passive.getRule().rule_id);
      t = chart.last_backpointer.get(active);
      if (t == null) {
        hasNext = false;
      } else {
        active = t.getActive();
        passive = t.getPassive();
      }
    }
    return result;
  }

  public List<Integer> get_grandchildren() {
    Edge passive = traversal.getPassive();

    Traversal t = chart.last_backpointer.get(passive);
    Edge current = t.getActive();
    if (current == null)
      return null;

    boolean hasNext = true;

    List<Integer> result = new ArrayList<>();
    while (hasNext) {
      Edge grandchild = t.getPassive();
      if (grandchild != null)
        result.add(grandchild.getRule().rule_id);
      t = chart.last_backpointer.get(current);
      if (t == null) {
        hasNext = false;
      } else {
        current = t.getActive();
      }
    }
    return result;
  }

  public String getLeftSibling() {
    Edge active = traversal.getActive();
    Edge passive = traversal.getPassive();

    if (active == null)
      return null;

    boolean hasNext = true;

    Traversal t;
    t = chart.last_backpointer.get(active);
    if (t == null) {
      hasNext = false;
    } else {
      active = t.getActive();
      passive = t.getPassive();
    }

    while (hasNext) {
      if (passive != null)
        return String.valueOf(passive.getRule().rule_id);
      t = chart.last_backpointer.get(active);
      if (t == null) {
        hasNext = false;
      } else {
        active = t.getActive();
        passive = t.getPassive();
      }
    }
    return "x";
  }

  public Token[] getSurfaceFromAnchor(int offset) {
    Edge result = traversal.getResult();
    if (!result.seenAnchor())
      return null;
    try {
      return input.get(result.getAnchorPosition()+offset);
    } catch (ArrayIndexOutOfBoundsException e) {
      return null;
    }
  }

}
