package edu.columbia.nlp.gbdp;

/**
 * Created by daniel on 7/12/16.
 */
public class DependencyNode {
  int position;
  int parent_position;
  public int right;
  String tree;

  public DependencyNode(String tree, int position) {
    this.tree = tree;
    this.position = position;
  }

  public void setParentPosition(int pos) {
    parent_position = pos;
  }

  public int getPosition(){
    return position;
  }
  public int getParentPosition() {
    return parent_position;
  }
  public String getTree() {
    return tree;
  }

  @Override
  public String toString() {
    return position+"-"+tree;
  }

}
