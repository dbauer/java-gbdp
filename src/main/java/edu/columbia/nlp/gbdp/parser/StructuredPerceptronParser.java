
package edu.columbia.nlp.gbdp.parser;

import edu.columbia.nlp.gbdp.Edge;
import edu.columbia.nlp.gbdp.SentenceWithGold;
import edu.columbia.nlp.gbdp.Traversal;
import edu.columbia.nlp.gbdp.exception.GrammarInitializationException;
import edu.columbia.nlp.gbdp.grammar.Grammar;
import edu.columbia.nlp.gbdp.perceptron_model.TraversalFeatureExtractor;
import edu.columbia.nlp.gbdp.perceptron_model.TraversalWithContext;
import edu.columbia.nlp.gbdp.taggers.Token;
import edu.columbia.nlp.util.averaged_perceptron.AveragedPerceptron;
import edu.columbia.nlp.util.averaged_perceptron.AveragedPerceptronModel;
import edu.columbia.nlp.util.averaged_perceptron.FeatureSet;
import edu.columbia.nlp.util.averaged_perceptron.SparseVector;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by bauer on 8/26/16.
 */
public class StructuredPerceptronParser extends AbstractParser {


  private static final boolean TRAIN_DEBUG = false;

  private AveragedPerceptron<TraversalWithContext> perceptron;
  private FeatureSet<TraversalWithContext> featureSet;

  public StructuredPerceptronParser(Grammar grammar, Properties config) throws IOException{
    super(grammar, config);

    featureSet = TraversalFeatureExtractor.makeFeatureSet(grammar.size());
    perceptron = new AveragedPerceptron<>(featureSet);
    perceptron.initLabel("");
  }

  public StructuredPerceptronParser(Grammar grammar, Properties config, AveragedPerceptronModel<TraversalWithContext> model) throws IOException {
    super(grammar, config);
    FeatureSet<TraversalWithContext> features = model.getFeatures();
    FeatureSet<TraversalWithContext> template = TraversalFeatureExtractor.makeFeatureSet(grammar.size());
    features.setExtractorsFromTemplate(template);
    perceptron = new AveragedPerceptron<>(features, model.getWeights());

  }

  private AveragedPerceptronModel<TraversalWithContext> read_model_from_file(File model_file) throws IOException{
    AveragedPerceptronModel<TraversalWithContext> model;
    try {
      FileInputStream is = new FileInputStream(model_file);
      ObjectInputStream ois = new ObjectInputStream(is);
      model = (AveragedPerceptronModel<TraversalWithContext>) ois.readObject();
    } catch (ClassNotFoundException e) {
      e.printStackTrace();
      throw new IOException();
    }
    return model;
  }


  public StructuredPerceptronParser(Grammar grammar, Properties config, File model_file) throws IOException {
    super(grammar, config);
    AveragedPerceptronModel<TraversalWithContext> model = read_model_from_file(model_file);
    System.err.println("Read model from "+model_file.getName());
    FeatureSet<TraversalWithContext> features = model.getFeatures();
    FeatureSet<TraversalWithContext> template = TraversalFeatureExtractor.makeFeatureSet(grammar.size());
    features.setExtractorsFromTemplate(template);
    perceptron = new AveragedPerceptron<>(features, model.getWeights());

  }


  @Override
  protected double scoreInitialEdge(Edge e) {
    Traversal dummy_traversal = new Traversal(null,null,e,0.0);
    //return 0.0;
    return perceptron.scoreAllNegative("", new TraversalWithContext(dummy_traversal, chart, tokens));
  }

  @Override
  protected double scoreTraversal(Traversal t) {
    return best_score.get(t.getActive()) + best_score.get(t.getPassive()) + perceptron.scoreAllNegative("", new TraversalWithContext(t, chart, tokens));
  }

  @Override
  protected double scoreNoAdjunction(Traversal t) {
    return best_score.get(t.getActive()) + perceptron.scoreAllNegative("", new TraversalWithContext(t, chart, tokens));
  }



  public boolean trainSentence(SentenceWithGold si) {

    Set<Traversal> traversals_in_gold = new HashSet<>(si.derivation.getTraversals());
    Set<Edge> edges_in_gold = traversals_in_gold.stream().map(Traversal::getResult).collect(Collectors.toSet());

    if (TRAIN_DEBUG) {
      System.out.println("___");
      for (Traversal t : traversals_in_gold)
        System.out.println(t);
      System.out.println("___");
    }

    // initialize per-sentence grammar
    try {
      this.tokens = si.sentence;
      if (TRAIN_DEBUG) {
        for (Token[] tok : this.tokens) {
          System.out.print(tok[0]);
          System.out.print(" ");
        }
        System.out.print(" ");
      }
      reset();
    } catch (GrammarInitializationException e) {
      //e.printStackTrace();
      //for (Token[] tok : this.tokens) {
      //  System.out.print(Arrays.toString(tok));
      //  System.out.print(" ");
      //}
      //System.out.print(" ");
      //System.err.println("Grammar cannot parse sentence.");
      return false;
    }
    addInitialEdges();

    int i =0 ;
    while (!finishing_agenda.empty()) { // main parser loop

      if (TRAIN_DEBUG) {
        System.out.println("---" + i++ + "---");
        System.out.println("Perceptron:" + perceptron.weights.get(""));
      }

      //System.out.println(finishing_agenda.printHeap());

      Edge next_edge = finishing_agenda.pollMax();

      if (TRAIN_DEBUG) System.out.println("visiting "+next_edge);

      if  (edges_in_gold.contains(next_edge)) { // ignore edges not in gold
        Traversal backpointer;
        boolean isPredict = false;

        if (chart.last_backpointer.containsKey(next_edge)) { // complete or null adjunction
          backpointer = chart.last_backpointer.get(next_edge);
        } else { // predict
          backpointer = new Traversal(null, null, next_edge, next_edge.getScore());
          isPredict = true;
        }

        if (TRAIN_DEBUG) System.out.println("best traversal was "+backpointer);
        SparseVector featureValues = featureSet.apply(new TraversalWithContext(backpointer, chart, tokens));
        if (TRAIN_DEBUG) System.out.println("features: "+featureValues);


        if (isPredict || traversals_in_gold.contains(backpointer)){
          //perceptron.updateWeights(featureValues, 1 * AveragedPerceptron.LEARNING_RATE);
          if (TRAIN_DEBUG) System.out.println("OKAY");
        } else { // The best incoming traversal for this item is wrong! Do an update.

          Edge next_next_edge = finishing_agenda.getMax();
          double diff = 1; //next_edge.score - next_next_edge.score;

          perceptron.updateWeights(featureValues, -diff * AveragedPerceptron.LEARNING_RATE);
          if (TRAIN_DEBUG) System.out.println("BAD");
          return false;
        }
      }

      finishEdge(next_edge); // continue parsing
      while (!exploration_agenda.empty()) {
        Traversal t = exploration_agenda.pollMax();

        SparseVector features = featureSet.apply(new TraversalWithContext(t, chart, tokens));
        double weight = perceptron.scoreAllNegative("",new TraversalWithContext(t, chart, tokens));
        if (TRAIN_DEBUG)  {
          System.out.println("Exploring "+ t + " "+weight);
          System.out.println(perceptron.weights.get(""));
          System.out.println(features);
        }
        exploreTraversal(t);
      }
    }

    return true;
  }



  public AveragedPerceptronModel<TraversalWithContext> train(Iterable<SentenceWithGold> data, int iterations) {

    for (int i=1; i<=iterations; i++) {

      int correct =0 ;
      int count = 0;

      for (SentenceWithGold si : data) {
        if (trainSentence(si)) correct++;
        if (count % 1000 == 0)
          System.out.print(".");
        count++;
      }
      System.out.println();
      System.out.println(String.format("Iteration %d: correctly parsed %d/%d sentences.", i, correct, count));
    }
    perceptron.average_weights();

    return new AveragedPerceptronModel<>(featureSet, perceptron.weights);

  }

}
