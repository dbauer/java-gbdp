package edu.columbia.nlp.gbdp;

import com.beust.jcommander.Parameter;

import java.util.List;

public class CommandLineOptions {

  @Parameter(names = "-gr", description = "Grammar")
  public String grammar;

  //@Parameter(names = "-n", description = "print n-best output.", required=false)
  //public int nbest = 1;

  @Parameter(names = "--beamn", description = "use beam search with beam width (max number of items on agenda) n.", required=false)
  public Integer finishingAgendaBeamN;

  @Parameter(names = "--stagn", description = "use n-best supertags.", required=false)
  public Integer superTagBeamN;

  @Parameter(names = "--stagratio", description = "use only supertags that are at least as good as r * the score of the best supertag", required=false)
  public Double superTagBeamRatio;

  @Parameter(names = "--perceptron", description = "Use a structured perceptron model", required=false)
  public String perceptronModelFile;

  @Parameter(names = "--stag-factored", description = "Use the supertag factored model")
  public boolean stagFactoredModel = false;

  @Parameter(description = "Input files. If no files are specified, read input from stdin.", variableArity=true)
  public List<String> inputFiles;

  @Parameter(names = "--help", help = true)
  public boolean help = false;

}