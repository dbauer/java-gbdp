package edu.columbia.nlp.util;

import org.junit.Test;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

/**
 * Created by daniel on 7/7/16.
 */
public class CartesianProductGeneratorTest {

  @Test
  public void testCartesianProductGenerator() {
    ArrayList<Integer> testList1 = new ArrayList(Arrays.asList(new Integer[]{1, 2, 3}));
    ArrayList<Integer> testList2 = new ArrayList(Arrays.asList(new Integer[]{4, 5}));
    ArrayList<Integer> testList3 = new ArrayList(Arrays.asList(new Integer[]{6}));
    ArrayList<ArrayList<Integer>> allLists = new ArrayList<>();
    allLists.add(testList1);
    allLists.add(testList2);
    allLists.add(testList3);

    CartesianProductGenerator<Integer> gen = new CartesianProductGenerator<>(allLists);

    ArrayList<List<Integer>> gold = new ArrayList<>();
    gold.add(Arrays.asList(new Integer[]{1,4,6}));
    gold.add(Arrays.asList(new Integer[]{2,4,6}));
    gold.add(Arrays.asList(new Integer[]{3,4,6}));
    gold.add(Arrays.asList(new Integer[]{1,5,6}));
    gold.add(Arrays.asList(new Integer[]{2,5,6}));
    gold.add(Arrays.asList(new Integer[]{3,5,6}));

    int i = 0;
    for (List<Integer> result : gen) {
      assertEquals(gold.get(i++), result);
    }
  }

}
