package edu.columbia.nlp.gbdp.perceptron_model;

import edu.columbia.nlp.gbdp.Chart;
import edu.columbia.nlp.gbdp.Edge;
import edu.columbia.nlp.gbdp.Traversal;
import edu.columbia.nlp.gbdp.grammar.NonterminalRuleToken;
import edu.columbia.nlp.gbdp.taggers.Token;
import edu.columbia.nlp.util.averaged_perceptron.FeatureSet;
import edu.columbia.nlp.util.averaged_perceptron.MultipleHotFeature;
import edu.columbia.nlp.util.averaged_perceptron.OneHotFeature;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

/**
 * Created by bauer on 8/26/16.
 */
public class TraversalFeatureExtractor  {

    public static FeatureSet<TraversalWithContext> makeFeatureSet(int grammar_size) {
        FeatureSet<TraversalWithContext> features = new FeatureSet<>();

        //OneHotFeature<TraversalWithContext, String> parent_lhs = new OneHotFeature<>(tc-> { Traversal t = tc.getTraversal(); return t.getActive() == null ? "x" : t.getActive().getRule().lhs;}, 100);
        //OneHotFeature<TraversalWithContext, String> child_lhs = parent_lhs.spawn(tc -> {Traversal t = tc.getTraversal(); return t.getPassive() == null ? "x" : t.getPassive().getRule().lhs;});

        //OneHotFeature<Traversal, String> parent_rule = new OneHotFeature<>( t-> t.getActive() == null ? "x" : String.valueOf(t.getActive().getRule().rule_id), 10000);
        //OneHotFeature<Traversal, String> child_rule = parent_rule.spawn(t -> t.getPassive() == null ? "x" : String.valueOf(t.getPassive().getRule().rule_id));


        //OneHotFeature<Traversal, String> parent_tree = new OneHotFeature<>(t -> t.getActive() == null ? "x" : t.getActive().getRule().anchor, 10000);
        //OneHotFeature<Traversal, String> child_tree = parent_tree.spawn(t-> t.getPassive() == null ? "x" : t.getPassive().getRule().anchor);

        OneHotFeature<TraversalWithContext, String> adjunction_count = new OneHotFeature<TraversalWithContext, String>(tc-> {
            Integer adjCount = tc.getAdjunctionCount();
            if (adjCount == null)
                return null;

            int parentrule_id = tc.getTraversal().getActive().getRule().rule_id;
            int parent_dot = tc.getTraversal().getActive().getDot();
            return parentrule_id+"_"+parent_dot+"_"+adjCount;
        }, 80000);

        OneHotFeature<TraversalWithContext, String> parenttree_childtree = new OneHotFeature<>(tc -> {
            Traversal t = tc.getTraversal();
            //System.out.println(t);
            if (t.getActive() == null && t.getPassive() == null) return null;
            //if (t.getActive() == null) System.out.println(t);
            if (t.getResult().getRule()==t.getActive().getRule() && t.getPassive()==null) return null;
            return t.getActive().getRule().rule_id+"_"+t.getPassive().getRule().rule_id;
        } ,400000);


        OneHotFeature<TraversalWithContext, String> transition_type = new OneHotFeature<>(tc -> {
            Traversal t = tc.getTraversal();
            Edge active = t.getActive();
            if (active != null) {
                String type = null;
                NonterminalRuleToken outside = (NonterminalRuleToken) active.getOutside();
                if (outside.hasKleene) {

                    tc.getChart().last_backpointer.get(active);
                    if (t.getPassive() == null)
                        type = "na";
                    else
                        type = "adj";
                } else {
                    type = "subst";
                }
                return type;
            }
            return null;
        }, 3);


        OneHotFeature<TraversalWithContext, String> leftOrRight = new OneHotFeature<>(tc -> {
            Traversal t = tc.getTraversal();
            Edge active = t.getActive();
            if (active != null) {
                NonterminalRuleToken outside = (NonterminalRuleToken) active.getOutside();
                if (outside.hasKleene && (t.getPassive() == null)) return null; // no result for null adjunction

                String position = active.seenAnchor() ? "r" : "l";
                String this_rule = String.valueOf(active.getRule().rule_id);
                String child_rule = String.valueOf(t.getPassive().getRule().rule_id);
                return position + "_" + this_rule + "_" + child_rule;
            }
            return null;
        }, 80000);


        OneHotFeature<TraversalWithContext, String> rule_and_lookahead1 = new OneHotFeature<>(tc -> {
            Traversal t = tc.getTraversal();
            List<Token[]> input = tc.getInput();
            Edge active = t.getActive();
            if (active != null) {
                int right = active.getRight();
                String lookahead = right < input.size() ? input.get(right)[0].stag : "END";
                Edge passive = t.getPassive();
                String passive_rule = (passive == null) ? "skip" : String.valueOf(passive.getRule().rule_id);

                String this_rule = String.valueOf(active.getRule().rule_id);

                return this_rule + "_" + passive_rule+"_"+lookahead;
            } else {
                return null;}

        }, 1000000);

        MultipleHotFeature<TraversalWithContext, String> rule_and_lookahead_stags = new MultipleHotFeature<>(tc -> {
            List<String> result = new ArrayList<>();
            Traversal t = tc.getTraversal();
            List<Token[]> input = tc.getInput();
            Edge active = t.getActive();
            if (active != null) {
                int right = active.getRight();
                Edge passive = t.getPassive();
                String passive_rule = (passive == null) ? "skip" : String.valueOf(passive.getRule().rule_id);
                String this_rule = String.valueOf(active.getRule().rule_id);

                if (right < input.size()) {
                    for (Token tok : input.get(right)) {
                        result.add(this_rule+"_"+passive_rule+"_"+tok.stag);
                    }
                    return result;
                } else {
                    result.add(this_rule+"_"+passive_rule+"_"+"END");
                    return result;
                }
            } else {
                return null;}

        }, 1000000);


        OneHotFeature<TraversalWithContext, String> rule_and_boundary = new OneHotFeature<>(tc -> {
            Traversal t = tc.getTraversal();
            List<Token[]> input = tc.getInput();
            Edge active = t.getActive();
            if (active != null) {
                int right = active.getRight()-1;

                String lookahead;
                if (right < 0) {
                    lookahead = "BEGIN";
                } else {
                    lookahead = input.get(right)[0].stag;
                }

                Edge passive = t.getPassive();
                String passive_rule = (passive == null) ? "skip" : String.valueOf(passive.getRule().rule_id);

                String this_rule = String.valueOf(active.getRule().rule_id);

                return this_rule + "_" + passive_rule+"_"+lookahead;
            } else {
                return null;}

        }, 1000000);


        OneHotFeature<TraversalWithContext, String> rule_and_lookahead2 = new OneHotFeature<>(tc -> {
            Traversal t = tc.getTraversal();
            List<Token[]> input = tc.getInput();
            Edge active = t.getActive();
            if (active != null) {
                int right = active.getRight();
                String lookahead = right < input.size() ? input.get(right)[0].stag : "END";
                String lookahead2 = (right+1) < input.size() ? input.get(right+1)[0].stag : "END";
                Edge passive = t.getPassive();
                String passive_rule = (passive == null) ? "skip" : String.valueOf(passive.getRule().rule_id);

                String this_rule = String.valueOf(active.getRule().rule_id);

                return this_rule + "_" + passive_rule+"_"+lookahead+"_"+lookahead2;
            } else {
                return null;}

        }, 400000);

        OneHotFeature<TraversalWithContext, String> stag_rule_and_word = new OneHotFeature<>(tc -> {
            Traversal t = tc.getTraversal();
            if (t.getActive()!=null || t.getPassive() != null)
                return null;

            Token[] anchor = tc.getSurfaceFromAnchor(0);
            if (anchor == null)
                return "_"+t.getResult().getRule().rule_id;
            else
                return anchor[0].word + "_"+ t.getResult().getRule().rule_id;

        }, 400000);

        OneHotFeature<TraversalWithContext, String> stag_rule_and_pos = new OneHotFeature<>(tc -> {
            Traversal t = tc.getTraversal();
            if (t.getActive()!=null || t.getPassive() != null)
                return null;

            Token[] anchor = tc.getSurfaceFromAnchor(0);
            if (anchor == null)
                return "_"+t.getResult().getRule().rule_id;
            else
                return anchor[0].pos + "_"+ t.getResult().getRule().rule_id;

        }, 400000);

        OneHotFeature<TraversalWithContext, String> stag_rule_and_posm1 = stag_rule_and_pos.spawn(tc -> {
            Traversal t = tc.getTraversal();
            if (t.getActive()!=null || t.getPassive() != null)
                return null;

            Token[] anchor = tc.getSurfaceFromAnchor(-1);
            if (anchor == null)
                return "_"+t.getResult().getRule().rule_id;
            else
                return anchor[-1].pos + "_"+ t.getResult().getRule().rule_id;
        });

        OneHotFeature<TraversalWithContext, String> stag_rule_and_posm2 = stag_rule_and_pos.spawn(tc -> {
            Traversal t = tc.getTraversal();
            if (t.getActive()!=null || t.getPassive() != null)
                return null;

            Token[] anchor = tc.getSurfaceFromAnchor(-2);
            if (anchor == null)
                return "_"+t.getResult().getRule().rule_id;
            else
                return anchor[-2].pos + "_"+ t.getResult().getRule().rule_id;
        });

        OneHotFeature<TraversalWithContext, String> stag_rule_and_posp1 = stag_rule_and_pos.spawn(tc -> {
            Traversal t = tc.getTraversal();
            if (t.getActive()!=null || t.getPassive() != null)
                return null;

            Token[] anchor = tc.getSurfaceFromAnchor(1);
            if (anchor == null)
                return "_"+t.getResult().getRule().rule_id;
            else
                return anchor[1].pos + "_"+ t.getResult().getRule().rule_id;
        });

        OneHotFeature<TraversalWithContext, String> stag_rule_and_posp2 = stag_rule_and_pos.spawn(tc -> {
            Traversal t = tc.getTraversal();
            if (t.getActive()!=null || t.getPassive() != null)
                return null;

            Token[] anchor = tc.getSurfaceFromAnchor(2);
            if (anchor == null)
                return "_"+t.getResult().getRule().rule_id;
            else
                return anchor[2].pos + "_"+ t.getResult().getRule().rule_id;
        });

        OneHotFeature<TraversalWithContext, String> stag_rule_and_postripple = new OneHotFeature<TraversalWithContext, String>(tc -> {
            Traversal t =  tc.getTraversal();
            if (t.getActive()!=null || t.getPassive() != null)
                return null;

            Token[] tm1 = tc.getSurfaceFromAnchor(-1);
            Token[] tok = tc.getSurfaceFromAnchor(0);
            Token[] tp1 = tc.getSurfaceFromAnchor(1);

            String pm1 = tm1 == null ? "x" : tm1[0].pos;
            String p = tok == null ? "x" : tok[0].pos;
            String pp1 = tp1 == null ? "x" : tp1[0].pos;

            return pm1 + "_"+ p +"_"+pp1 + t.getResult().getRule().rule_id;
        }, 400000);

        //features.addVectorFeature("adj_or_subst", adj_or_subst);
        //features.addVectorFeature("na", adj_or_subst);
        //features.addVectorFeature("parent_lhs", parent_lhs);
        //features.addVectorFeature("child_lhs", child_lhs);
        //features.addVectorFeature("parent_rule", parent_rule);
        //features.addVectorFeature("child_rule", child_rule);
        //features.addVectorFeature("parent_tree", parent_tree);
        //features.addVectorFeature("child_tree", child_tree);
        //features.addVectorFeature("parent_lhs", parent_lhs);
        //features.addVectorFeature("child_lhs", child_lhs);

        //features.addVectorFeature("transition_type", transition_type);
        features.addVectorFeature("parent_child", parenttree_childtree);
        //features.addMultiHotFeature("rule_and_lookahead_stags", rule_and_lookahead_stags);
        //features.addMultiHotFeature("rule_and_lookahead", rule_and_lookahead_stags);
        //features.addVectorFeature("rule_and_boundary", rule_and_boundary);
        features.addVectorFeature("adjunction_count", adjunction_count);
        //features.addVectorFeature("rule_and_lookahead2", rule_and_lookahead2);

        features.addVectorFeature("rule_and_word", stag_rule_and_word);
        features.addVectorFeature("rule_and_pos", stag_rule_and_pos);
        features.addVectorFeature("rule_and_posm1", stag_rule_and_posm1);
        features.addVectorFeature("rule_and_posm2", stag_rule_and_posm2);
        features.addVectorFeature("rule_and_posp1", stag_rule_and_posm1);
        features.addVectorFeature("rule_and_posp2", stag_rule_and_posm2);
        features.addVectorFeature("rule_and_postripple", stag_rule_and_postripple);

        return features;
    }

}