package edu.columbia.nlp.gbdp.parser;

import edu.columbia.nlp.gbdp.Edge;
import edu.columbia.nlp.gbdp.Traversal;
import edu.columbia.nlp.gbdp.grammar.NonterminalRuleToken;
import edu.columbia.nlp.gbdp.grammar.Grammar;

import java.io.IOException;
import java.util.Properties;

/**
 * Created by bauer on 10/27/16.
 */
public class PcfgParser extends AbstractParser {

  public PcfgParser(Grammar grammar, Properties config) throws IOException {
    super(grammar, config);
  }

  @Override
  protected double scoreInitialEdge(Edge edge) {
    return edge.getRule().weight;
  }

  @Override
  protected double scoreTraversal(Traversal t) {
    return  t.getResult().getScore() + best_score.get(t.getActive()) + best_score.get(t.getPassive());
  }
  protected double scoreNoAdjunction(Traversal t) {
    NonterminalRuleToken outside = (NonterminalRuleToken) t.getActive().getOutside();
    return t.getResult().getScore() + best_score.get(t.getActive()) + grammar.getNoAdjScore(outside.value);
  }


}
