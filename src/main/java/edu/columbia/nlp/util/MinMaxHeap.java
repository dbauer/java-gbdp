package edu.columbia.nlp.util;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by daniel on 6/29/16.
 */
public class MinMaxHeap<T extends Comparable<? super T>> implements DoubleEndedPriorityQueue<T>{

  private int last_pos;
  private T[] heap_array;

  private final static int DEFAULT_SIZE = 20;

  public int max_beam_size;

  public MinMaxHeap(){
    max_beam_size = 0;
    last_pos = 1;
    heap_array = (T[]) new Comparable[DEFAULT_SIZE+1];
  }

  public MinMaxHeap(int size){
    last_pos = 1;
    heap_array = (T[]) new Comparable[size+1];
  }


  public ArrayList<T> asArrayList() {
    ArrayList<T> result = new ArrayList<T>(last_pos);
    for (int i=1; i<last_pos; i++) {
        result.add(heap_array[i]);
    }
    return result;
  }

  public ArrayList<T> getSortedArrayList() {
    ArrayList<T> result = new ArrayList<T>(last_pos);
    while (!empty()) {
      result.add(pollMax());
    }
    return result;
  }

  /**
   * Efficient computation of floor log2
   * @param i
   */
  private int floorLogTwo(int i) {
    if (i <= 0) throw new IllegalArgumentException();
    return 31 - Integer.numberOfLeadingZeros(i);
  }

  /**
   * Check if a given integer is on a max or a min level
   * @param position
   * @return
   */
  public boolean isMaxLevel(int position) {
    return (floorLogTwo(position) & 1) != 0;
  }

  /**
   * Recursively bubble up the item at position j until it is in the
   * correct position.
   * @param j
   */
  private void percolateUp(int j) {
    T the_item = heap_array[j];
    int parent_pos = j / 2;
    if (isMaxLevel(j)) { // Max level
      if (j > 1 && (the_item.compareTo(heap_array[parent_pos]) < 0)) {
        heap_array[j] = heap_array[parent_pos];
        heap_array[parent_pos] = the_item;
        percolateUpMin(parent_pos);
      } else {
        percolateUpMax(j);
      }
    } else { // Min level
      if (j > 1 && (the_item.compareTo(heap_array[parent_pos]) > 0)){
        heap_array[j] = heap_array[parent_pos];
        heap_array[parent_pos] = the_item;
        percolateUpMax(parent_pos);
      } else {
        percolateUpMin(j);
      }
    }
  }

  private void percolateUpMin(int j) {
    T the_item = heap_array[j];
    if (floorLogTwo(j)>=2) { // j has a grandparent
      int grandparent_pos = j / 4;
      if (the_item.compareTo(heap_array[grandparent_pos]) < 0) {
        heap_array[j] = heap_array[grandparent_pos];
        heap_array[grandparent_pos] = the_item;
        percolateUpMin(grandparent_pos);
      }
    }
  }

  private void percolateUpMax(int j) {
    T the_item = heap_array[j];
    if (floorLogTwo(j)>=2) { // j has a grandparent
      int grandparent_pos = j / 4;
      if (the_item.compareTo(heap_array[grandparent_pos]) > 0) {
        heap_array[j] = heap_array[grandparent_pos];
        heap_array[grandparent_pos] = the_item;
        percolateUpMax(grandparent_pos);
      }
    }
  }

  private void percolateDown(int j) {
    if (isMaxLevel(j))
      percolateDownMax(j);
    else
      percolateDownMin(j);
  }

  private int[] getChildGrandchildPositions(int j) {
    int[] positions = new int[7];
    positions[0] = j;
    int left_child = positions[1] = j*2;
    positions[2] = j*2+1;
    for (int i=0;i<4;i++)
      positions[3+i] = (left_child * 2) + i;
    return positions;
  }

  private int smallestChildOrGrandchild(int j) {
    int[] positions = getChildGrandchildPositions(j);
    int smallest_pos = j;
    T smallest_item = heap_array[j];
    for (int i=0; i < positions.length && positions[i] < last_pos ; i++) {
      if (heap_array[positions[i]].compareTo(smallest_item) < 0) {
        smallest_pos = positions[i];
        smallest_item = heap_array[smallest_pos];
      }
    }
    return smallest_pos;
  }

  private int largestChildOrGrandchild(int j) {
    int[] positions = getChildGrandchildPositions(j);
    int largest_pos = j;
    T largest_item = heap_array[j];
    for (int i=0; i < positions.length && positions[i] < last_pos ; i++) {
      if (heap_array[positions[i]].compareTo(largest_item) > 0) {
        largest_pos = positions[i];
        largest_item = heap_array[largest_pos];
      }
    }
    return largest_pos;
  }

  private void percolateDownMin(int j) {
    T the_item = heap_array[j];
    int smallest_pos = smallestChildOrGrandchild(j);
    if (smallest_pos != j) {
      heap_array[j] = heap_array[smallest_pos];
      heap_array[smallest_pos] = the_item;
      if (smallest_pos > j * 2 + 1) { // smallest was a grandchild
        T smallest_item = heap_array[j];
        if (the_item.compareTo(heap_array[smallest_pos/2]) > 0) {
          heap_array[smallest_pos] = heap_array[smallest_pos/2];
          heap_array[smallest_pos/2] = the_item;
        }
      }
      percolateDownMin(smallest_pos);
    }
  }

  private void percolateDownMax(int j) {
    T the_item = heap_array[j];
    int largest_pos = largestChildOrGrandchild(j);
    if (largest_pos != j) {
      heap_array[j] = heap_array[largest_pos];
      heap_array[largest_pos] = the_item;
      if (largest_pos > j * 2 + 1) { // smallest was a grandchild
        if (the_item.compareTo(heap_array[largest_pos/2]) < 0) {
          heap_array[largest_pos] = heap_array[largest_pos/2];
          heap_array[largest_pos/2] = the_item;
        }
        percolateDownMax(largest_pos);
      }
    }
  }

  /**
   * Make sure there is still enough space in the heap for another item.
   * Allocate more space if necessary.
   */
  private void ensureSize() {
    int current_size = heap_array.length;
    if (last_pos==current_size-1) {
      T[] tmp = heap_array;
      heap_array = (T[]) new Comparable[current_size * 2 + 1];
      for (int i=1; i<current_size; i++) {
        heap_array[i] = tmp[i];
      }

    }
  }

  public void add(T t) {
    ensureSize();
    heap_array[last_pos] = t;
    percolateUp(last_pos);
    last_pos++;
    max_beam_size = size() > max_beam_size ? size() : max_beam_size;
  }

  public T getMin() {
    if (last_pos == 1)
      return null;
    return heap_array[1];
  }

  public int getMaxPos() {
    if (last_pos == 1)
      throw new RuntimeException("Heap was empty");
    if (last_pos == 2)
      return 1;
    if (last_pos == 3)
      return 2;
    return heap_array[2].compareTo(heap_array[3]) > 0 ? 2 : 3;
  }

  public T getMax() {
    try {
      return heap_array[getMaxPos()];
    } catch (RuntimeException e) {
      return null;
    }
  }

  public T pollMin() {
    T root = getMin();
    heap_array[1] = heap_array[--last_pos];
    heap_array[last_pos] = null;
    percolateDown(1);
    return root;
  }

  public T pollMax() {
    int max_pos = getMaxPos();
    T max_item = heap_array[max_pos];
    heap_array[max_pos] = heap_array[--last_pos];
    heap_array[last_pos] = null;
    percolateDown(max_pos);
    return max_item;
  }

  public int size() {
    return last_pos-1;
  }

  public boolean empty() {
    return last_pos == 1;
  }

  public String printHeap() {
    return Arrays.toString(heap_array);
  }




}
