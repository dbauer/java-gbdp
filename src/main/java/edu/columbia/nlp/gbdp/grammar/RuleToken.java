package edu.columbia.nlp.gbdp.grammar;

/**
 * A single token of a rule RHS
 * Created by daniel on 6/13/16.
 */
public abstract class RuleToken {
  public String value;

  public RuleToken(String value) {
    this.value = value;
  }


  @Override
  public String toString() {
    return value;
  }

}
