package edu.columbia.nlp.gbdp;

import edu.columbia.nlp.gbdp.exception.ParserException;
import edu.columbia.nlp.util.averaged_perceptron.DataInstance;

/**
 * A traversal stores a possible transition made by the parser. This is an immutable class.
 * Created by daniel on 6/13/16.
 */
public class Traversal extends HasScore implements DataInstance{

  private final Edge active; // the active edge that's the source
  private final Edge passive; // the passive edge that combines with the active edge
  private final Edge result;
  private final int hash_cache;

  public Traversal(Edge active, Edge passive, Edge result, double score) throws ParserException {
    //if (!active.isActive())
    //  throw new PasrserException(
    //          String.format("Error creating Traversal: Edge %s is not active.", active.toString()));
    //if (passive!=null && passive.isActive())
    //  throw new ParserException(
    //          String.format("Error creating Traversal: Edge %s is not passive.", passive.toString()));

    this.active = active;
    this.passive = passive;
    this.result = result;

    int passiveHash = passive == null ? 137 : 97 * passive.hashCode();
    int activeHash = active == null ? 47 : 61 * active.hashCode();

    int resultHash = 23 * result.hashCode();

    this.hash_cache =  activeHash + passiveHash + resultHash;
    this.score = score; //Math.pow(score,(1/(result.getRight()-result.getLeft()+1)));
  }


  public Edge getActive() {
    return active;
  }
  public Edge getPassive(){
    return passive;
  }

  @Override
  public int hashCode(){
    return hash_cache;
  }

  @Override
  public boolean equals(Object other) {
    if (!(other instanceof Traversal))
      return false;
    Traversal otherTraversal = (Traversal) other;

    if (this.active != null && otherTraversal.active != null) {
      if (!this.active.equals(otherTraversal.active))
        return false;
    } else if (this.active != otherTraversal.active) // true only of both of them are null
      return false;

    if (this.passive != null && otherTraversal.passive != null) {
      if (!this.passive.equals(otherTraversal.passive))
        return false;
    } else if (this.passive != otherTraversal.passive) // true only of both of them are null
      return false;

    return this.result.equals(otherTraversal.result);
  }

  @Override
  public String toString() {
    return String.format("[%s,%s | %s] (%f)", active, passive, result, score);
  }

  public Edge getResult() {
    return result;
  }

  @Override
  public String getLabel() {
    return null;
  }

  @Override
  public String getDescription() {
    return null;
  }
}
