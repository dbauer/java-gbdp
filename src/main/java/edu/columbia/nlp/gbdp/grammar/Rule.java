package edu.columbia.nlp.gbdp.grammar;

import java.util.List;

/**
 * A basic grammar rule.
 * Created by daniel on 6/13/16.
 */
public class Rule {
  public int rule_id;
  public String lhs;
  public List<RuleToken> rhs;
  public double weight;

  public int terminal_count = 0;
  public int nonterminal_count = 0;
  public int kleene_count = 0;

  public String anchor;

  public int anchorPos;

  public Rule(String lhs, List<RuleToken> rhs, double weight) {
    this.lhs = lhs;
    this.rhs = rhs;
    this.weight = weight;
  }
  public Rule(String lhs, List<RuleToken> rhs, double weight, int rule_id) {
    this.lhs = lhs;
    this.rhs = rhs;
    this.weight = weight;
    this.rule_id = rule_id;
    do_rhs_count();
  }

  private void do_rhs_count(){
    int count = 0;
    for (RuleToken t : rhs) {
      if (t instanceof TerminalRuleToken) {
        if (terminal_count == 0) { // Hack: use the first terminal as the lexical anchor.
          anchor = t.value;
          anchorPos = count;
        }
        terminal_count++;
        count += 1;
      }
      else {
        NonterminalRuleToken nt = (NonterminalRuleToken) t;
        if (nt.hasKleene)
          kleene_count++;
        else
          nonterminal_count++;
      }
    }
  }

  /**
   * Return the number of symbols on the rule RHS.
   */
  public int size() {
    return rhs.size();
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append(lhs);
    sb.append(" ->");
    for (RuleToken t : rhs) {
      sb.append(" ");
      sb.append(t);
    }
    sb.append("; ");
    sb.append(weight);
    return sb.toString();
  }

  @Override
  public boolean equals(Object other) {
    if (!(other instanceof Rule))
      return false;
    return rule_id == ((Rule) other).rule_id;
  }
  public boolean hasAnchor() {
    return anchorPos!=-1;
  }

}
