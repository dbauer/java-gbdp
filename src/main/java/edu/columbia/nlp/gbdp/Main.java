package edu.columbia.nlp.gbdp;

import com.beust.jcommander.JCommander;
import edu.columbia.nlp.gbdp.forest.Node;
import edu.columbia.nlp.gbdp.grammar.Grammar;
import edu.columbia.nlp.gbdp.parser.AbstractParser;
import edu.columbia.nlp.gbdp.parser.PcfgParser;
import edu.columbia.nlp.gbdp.parser.StructuredPerceptronParser;
import edu.columbia.nlp.gbdp.parser.SupertagFactoredParser;
import edu.columbia.nlp.gbdp.taggers.Supertagger;
import edu.columbia.nlp.gbdp.taggers.Token;

import java.io.*;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.Duration;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.*;
import java.util.function.Consumer;
import java.util.stream.Stream;

/**
 * This is the main entry point for the program.
 * Created by daniel on 6/14/16.
 */
public class Main {





  public static Grammar loadGrammar(String gr_filename) {
    Grammar grammar = null;
    try {
      InputStream is = new FileInputStream(new File(gr_filename));
      grammar = Grammar.readGrammar(is);
      System.err.println(String.format("Read %d rules from %s.", grammar.size(), gr_filename));
    } catch (FileNotFoundException e) {
      System.err.println(String.format("Could not load grammar from %s. File not found.", gr_filename));
      e.printStackTrace();
      System.exit(1);
    } catch (IOException e) {
      System.err.println(String.format("Could not load grammar from %s. IO Exception. ", gr_filename));
      e.printStackTrace();
      System.exit(1);
    }
    return grammar;
  }

  public static void main(String[] args) throws IOException {
    CommandLineOptions commands = new CommandLineOptions();
    JCommander jCommander = new JCommander(commands, args);
    jCommander.setProgramName("Grammar Based Dependency Parser (GBDP)");
    if (commands.help) {
      jCommander.usage();
      System.exit(0);
    }


    // Verify command line arguments
    if (commands.grammar == null) {
      System.err.println("No grammar file specified. Use -gr [grammar file]");
      System.exit(1);
    }

    Properties config = new Properties();
    config.setProperty("grammarFile", commands.grammar);

    /* DEPRECATED
      if (commands.t2tprobs != null) {
      config.setProperty("t2tprobs", commands.t2tprobs);
      }
    */

    if (commands.finishingAgendaBeamN != null) {
      config.setProperty("finishingAgendaBeamN", Integer.toString(commands.finishingAgendaBeamN));
      System.err.println("Use finishing agenda beam search with width "+config.getProperty("finishingAgendaBeamN"));
    }

    if (commands.perceptronModelFile != null) {
      config.setProperty("perceptronModelFile", commands.perceptronModelFile);
    }

    // Now read the grammar
    Grammar grammar = loadGrammar(commands.grammar);

    AbstractParser parser;


    //final ForestScorer scorer = new ForestScorer(grammar);

    if (commands.inputFiles == null || commands.inputFiles.isEmpty()) {
      // read input from stdin
      System.out.println("Need to specify at least one input file.");
    } else {
      for (String inputFile : commands.inputFiles) {

        try (Stream<String> stream = Files.lines(Paths.get(inputFile))) {


          stream.forEach(new Consumer<String>() {
            @Override
            public void accept(String s) {

              final Duration timeout = Duration.ofSeconds(30);
              ExecutorService executor = Executors.newSingleThreadExecutor();

              final Future<Node> handler = executor.submit(new Callable() {
                @Override
                public Node call() throws Exception {
                  List<Token[]> input;
                  if (commands.superTagBeamRatio != null)
                    input = Supertagger.parseNbestBeamSupertaggedLine(s, 10, commands.superTagBeamRatio);
                  else if (commands.superTagBeamN != null)
                    input = Supertagger.parseNbestSupertaggedLine(s,commands.superTagBeamN);
                  else
                    input = PcfgParser.parseGoldStagLine(s);

                  AbstractParser p;
                  if (commands.perceptronModelFile != null) {
                    File model_file = new File(commands.perceptronModelFile);
                    p= new StructuredPerceptronParser(grammar, config, model_file);
                  } else if (commands.stagFactoredModel){
                    p= new SupertagFactoredParser(grammar, config);
                    System.out.println("Using supertag factored model.");
                  } else {
                    p= new PcfgParser(grammar, config);
                  }

                  return p.parse(input);
                }
              });

              try {
                Node result = handler.get(timeout.toMillis(), TimeUnit.MILLISECONDS);
                if (result == null) {
                  System.out.println("# No parses found.");
                } else {
                  int i = 1;
                  System.out.println(String.format("# N-best: %d  Score: %f", i++, result.getScore()));
                  System.out.println("# " + result.printTree());
                  System.out.print(new DependencyTree(result).micaRepresentation());
                  System.out.println("...END...");
                }
              } catch (TimeoutException e) {
                handler.cancel(true);
                System.out.println("# Timeout.");

              } catch (InterruptedException e) {
                handler.cancel(true);
                System.out.println("# Interrupted Exception.");

              } catch (ExecutionException e) {
                handler.cancel(true);
                e.printStackTrace();
                System.out.println("# Execution Exception.");
                System.exit(1);
              }

              System.out.println("...EOS...\n...EOS...");
              System.gc();
            }
          });
        } catch (IOException e) {
          System.out.println(String.format("Error reading from input file %s", inputFile));
          e.printStackTrace();
          System.exit(1);
        }
      }
    }

  }

}

