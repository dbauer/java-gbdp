package edu.columbia.nlp.gbdp.grammar;

/**
 * A single terminal token on a rule RHS
 * Created by daniel on 6/13/16.
 */
public class TerminalRuleToken extends RuleToken {

  public TerminalRuleToken(String value) {
    super(value);
  }


}
