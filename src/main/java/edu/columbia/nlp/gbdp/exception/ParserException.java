package edu.columbia.nlp.gbdp.exception;

/**
 * Created by daniel on 6/13/16.
 */
public class ParserException extends RuntimeException {
  public ParserException(String s) {
    super(s);
  }
}
