package edu.columbia.nlp.util.averaged_perceptron;

/**
 * Created by bauer on 8/30/16.
 */
public class FeatureDimensionsExceededError extends RuntimeException {


  FeatureDimensionsExceededError() {
    this("");
  }

  FeatureDimensionsExceededError(String e) {
    super(e);
  }

}
