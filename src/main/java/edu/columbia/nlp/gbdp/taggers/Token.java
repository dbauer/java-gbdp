package edu.columbia.nlp.gbdp.taggers;

import edu.columbia.nlp.gbdp.HasScore;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Stream;

/**
 * Created by daniel on 7/18/16.
 */

public class Token extends HasScore {
    public String word;
    public String pos;
    public String stag;

    public Token(String word, String pos, String stag) {
      this.word = word;
      this.pos = pos;
      this.stag = stag;
    }

  public Token(String word, String pos, String stag, double score) {
    this(word, pos, stag);
    setScore(score);
  }


    public Token(String word) {
      this(word, null, null);
    }

  static List<Token[]> getNgrams(int n, List<Token> tokens) {
      Token[] tok_array = tokens.toArray(new Token[tokens.size()]);
      List<Token[]> result = new ArrayList<>();
      for (int i=0; i<tokens.size()+n-1; i++) {
        if (i-n+1 < 0) {
          Token[] ngram = new Token[n];
          for(int j=0; j<n;j++) {
            if (i-n+1+j < tok_array.length)
              ngram[j] = ((i - n + 1 + j) < 0) ? null : tok_array[i - n + 1 + j];
            else
              ngram[j] = null;
          }
          result.add(ngram);
        } else {
          result.add(Arrays.copyOfRange(tok_array, i - n + 1, i+1));
        }
      }
      return result;
    }


  public static List<Token> parseLine(String line) {
    List<Token> result = new ArrayList<Token>();
    for (String tok : line.split("\\s+")) {
      String[] parts = tok.split("\\|\\|");
      if (parts.length != 3)
        throw new RuntimeException(String.format("Invalid input token %s.", tok));
      result.add(new Token(parts[0],parts[1],parts[2]));
    }
    return result;
  }

  public static List<List<Token[]>> readInputWithTags(InputStream input_file) throws FileNotFoundException, IOException {

    BufferedReader br = new BufferedReader(new InputStreamReader(input_file));
    String line;

    List<List<Token[]>> result = new LinkedList<>();
    while ((line = br.readLine()) != null) {

      List<Token[]> toksInArray = new ArrayList<>();
      for (Token t : parseLine(line)) {
        Token[] newArr = new Token[1];
        newArr[0]=t;
        toksInArray.add(newArr);
      }
      result.add(toksInArray);
    }
    return result;
  }

  @Override
    public String toString() {
      return String.format("%s||%s||%s(%g)", word, pos, stag, getScore());
    }



  }
