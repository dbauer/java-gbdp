package edu.columbia.nlp.util.averaged_perceptron;
import org.junit.Test;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

/**
 * Created by daniel on 7/20/16.
 */
public class AveragedPerceptronTest {

  @Test
  public void testAveragedPerceptron() {
    ClassificationInstance i1 = new ClassificationInstance("A",new double[]{1, 0, 1, 0});
    ClassificationInstance i2 = new ClassificationInstance("B",new double[]{0, 1, 0, 1});
    ClassificationInstance i3 = new ClassificationInstance("A",new double[]{0, 0, 1, 1});
    ClassificationInstance i4 = new ClassificationInstance("B",new double[]{0, 1, 0, 0});
    List<ClassificationInstance> train = Arrays.asList(new ClassificationInstance[]{i1,i2,i3});
    AveragedPerceptron ap = new AveragedPerceptron();
    Map<String, Map<Integer,Float>> model = ap.train(train,20);
    System.out.println(model);
  }

}
