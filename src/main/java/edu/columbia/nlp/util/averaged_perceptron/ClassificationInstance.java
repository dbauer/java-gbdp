package edu.columbia.nlp.util.averaged_perceptron;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by daniel on 7/15/16.
 */
public class ClassificationInstance {

  private SparseVector values;
  private String label;
  private String description;

  public ClassificationInstance(String label) {
    this.label = label;
    values = new SparseVector();
  }

  public ClassificationInstance(String label, SparseVector featureValues) {
    this.label = label;
    this.values = featureValues;
  }

  public ClassificationInstance(String label, SparseVector features, String description) {
    this(label, features);
    this.description = description;
  }

  public ClassificationInstance(String label, double[] featureValues) {
    this(label);
    for (int i=0; i<featureValues.length;i++) {
      if (featureValues[i]>0)
        this.values.put(i, featureValues[i]);
    }
  }

  public SparseVector getFeatureValues(){
    return values;
  }

  public String getLabel(){
    return label;
  }

  public String getDescription(){
    return description;
  }

}
