package edu.columbia.nlp.gbdp.grammar;

import edu.columbia.nlp.gbdp.exception.GrammarInitializationException;
import edu.columbia.nlp.gbdp.grammar.gdgparser.ParseException;
import edu.columbia.nlp.gbdp.grammar.gdgparser.GdgParser;
import edu.columbia.nlp.gbdp.taggers.Token;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.*;

/**
 * Created by daniel on 6/14/16.
 */
public class Grammar {

  List<Rule> rules;
  private Map<String, List<Rule>> lhs_to_rules;
  Map<String, List<Rule>> terminal_to_rules;
  Map<String, Set<Rule>> rhs_symbol_to_rules;
  Map<String, Double> noadj_scores;
  List<Rule> nonlexical_rules;
  public String startsymbol;

  private void index_rule(Rule rule) {
    // LHS lookup
    String lhs = rule.lhs;
    if (!lhs_to_rules.containsKey(lhs))
      lhs_to_rules.put(lhs, new LinkedList<Rule>());

    if (rule.rhs.size() == 0) {
      noadj_scores.put(rule.lhs, rule.weight);
    } else {
      lhs_to_rules.get(lhs).add(rule);
      // terminal lookup
      boolean is_lexical = false;
      for (RuleToken t : rule.rhs) {
        String symbol = t.value;
        if (t instanceof TerminalRuleToken) {
          is_lexical = true;
          if (!terminal_to_rules.containsKey(symbol))
            terminal_to_rules.put(symbol, new LinkedList<>());
          terminal_to_rules.get(symbol).add(rule);

          if (!rhs_symbol_to_rules.containsKey(symbol))
            rhs_symbol_to_rules.put(symbol, new HashSet<>());
          rhs_symbol_to_rules.get(symbol).add(rule);

        } else {
          NonterminalRuleToken nt = (NonterminalRuleToken) t;
          if (!nt.hasKleene) {
            if (!rhs_symbol_to_rules.containsKey(symbol))
              rhs_symbol_to_rules.put(symbol, new HashSet<>());
            rhs_symbol_to_rules.get(symbol).add(rule);
          }

        }

      }
      if (!is_lexical) {
        nonlexical_rules.add(rule);
      }
    }
  }

  public List<Rule> rules_for_lhs(String lhs) {

    List<Rule> res = lhs_to_rules.get(lhs);
    if (res == null) {
      return new LinkedList<>();
    } else {
      return res;
    }
  }


  public Grammar(List<Rule> rule_list) {

    this.rules = new ArrayList(rule_list.size());
    lhs_to_rules = new HashMap<>();
    terminal_to_rules = new HashMap<>();
    nonlexical_rules = new LinkedList<>();
    rhs_symbol_to_rules = new HashMap<>();
    noadj_scores = new HashMap<>();

    // Copy all the rules, give them a rule id,
    // and add rules to indices for faster lookup.
    int rule_id = 0;
    for (Rule r : rule_list) {
      Rule rule = new Rule(r.lhs, r.rhs, r.weight, rule_id++);
      this.rules.add(rule);
      index_rule(rule);
    }
    startsymbol = rules.get(0).lhs;
  }

  public Grammar() {
    this.rules = new ArrayList();
    lhs_to_rules = new HashMap<String, List<Rule>>();
    terminal_to_rules = new HashMap<String, List<Rule>>();
    rhs_symbol_to_rules = new HashMap<>();
    nonlexical_rules = new LinkedList<>();
  }

  public int size() {
    return rules.size();
  }

  public Rule getRule(int rule_id) {
    return rules.get(rule_id);
  }

  public double getNoAdjScore(String symbol) {
    return noadj_scores.get(symbol);
  }

  public static Grammar readGrammar(InputStream is) throws IOException {
    try {

      BufferedReader br = new BufferedReader(new InputStreamReader(is));

      List<Rule> rules = new LinkedList<>();
      String line;
      while ((line = br.readLine()) != null) {
        if (!line.trim().isEmpty()) {
          InputStream stream = new ByteArrayInputStream(line.getBytes(StandardCharsets.UTF_8));
          GdgParser parser = new GdgParser(stream);
          rules.add(parser.Line());
        }
      }

      return new Grammar(rules);

    } catch (ParseException e) {
      throw new RuntimeException("Error parsing grammar:", e);
    }
  }
/*
   public Grammar getSentenceGrammar2(List<Token[]> tokens) {
      Grammar res = new Grammar();
      res.startsymbol = startsymbol;
      Set<Rule> seen_rules = new HashSet<>();

      Set<String> all_tokens = new HashSet<>();
      for (Token[] t : tokens)
        for (Token stag_option : t)
          all_tokens.add(stag_option.stag);

      for (Token[] tok : tokens) { // Find rules for each input token
        boolean foundToken = false;
        for (Token stag_option : tok) {
          List<Rule> rules_for_terminal = terminal_to_rules.get(stag_option.stag);
          if (rules_for_terminal != null) {
            for (Rule r : rules_for_terminal) { // check that all RHS tokens are in the input
              boolean inputHasRuleTokens = true;
              for (RuleToken rt : r.rhs) {
                if (rt instanceof TerminalRuleToken)
                  if (!all_tokens.contains(rt.value))
                    inputHasRuleTokens = false;
              }
              if (inputHasRuleTokens) {
                foundToken = true;
                if (!seen_rules.contains(r)) { // add rule
                  res.rules.add(r);
                  res.index_rule(r);
                  seen_rules.add(r);
                }
              }
            }
          }
        }
        if (!foundToken) { // fail if there is a single input word
          //System.err.println(Arrays.toString(tok) + " was not covered");
          throw new GrammarInitializationException();
        }

      }

      for (Rule r : nonlexical_rules) {
        res.rules.add(r);
        res.index_rule(r);
      }
      return res;
    }
*/
  public HashMap<Rule, Integer> getNeededSymbolsMap() {
    HashMap<Rule, Integer> needed_symbols = new HashMap<>();
    for (Rule r : rules) {
      int needed_for_rule = 0;
      HashSet<String> seenSymbols = new HashSet<>();
      for (RuleToken rt : r.rhs) {
        if (!seenSymbols.contains(rt.value)) {
          seenSymbols.add(rt.value);
          if (rt instanceof TerminalRuleToken)
            needed_for_rule += 1;
          else {
            NonterminalRuleToken nt = (NonterminalRuleToken) rt;
            if (!nt.hasKleene)
              needed_for_rule += 1;
          }
        }
      }
      needed_symbols.put(r, needed_for_rule);
    }
    return needed_symbols;
  }

  /**
   * Performs a simple reachability analysis by performing topological sorting on the rules.
   *
   * @param tokens
   * @return
   */
  public Grammar getSentenceGrammar(List<Token[]> tokens) {
    Grammar res = new Grammar();
    res.startsymbol = startsymbol;

    Set<String> seen_symbols = new HashSet<>();
    HashMap<Rule, Integer> needed_symbols = getNeededSymbolsMap();

    Queue<String> todo = new LinkedList<String>();
    // Add rules for nonterminals.
    for (Token[] toks : tokens) {
      for (Token t : toks) {
        String symbol = t.stag;
        if (!seen_symbols.contains(symbol)) {
          seen_symbols.add(symbol);
          if (!terminal_to_rules.containsKey(symbol))
            throw new GrammarInitializationException();
          for (Rule r : terminal_to_rules.get(symbol)) {
            Integer left = needed_symbols.put(r, needed_symbols.get(r) - 1) - 1;
            if (left == 0) {
              res.rules.add(r);
              res.index_rule(r);
              if (!seen_symbols.contains(r.lhs)) {
                seen_symbols.add(r.lhs);
                todo.add(r.lhs);
              }
            }
          }
        }
      }
    }

    boolean foundStartsymbol = false;
    while (!todo.isEmpty()) {
      String symbol = todo.poll();
      if (!symbol.equals(startsymbol)) {
        if (rhs_symbol_to_rules.containsKey(symbol)) { // Some LHS symbols do not appear without a * on a RHS
          for (Rule r : rhs_symbol_to_rules.get(symbol)) {
            Integer left = needed_symbols.put(r, needed_symbols.get(r) - 1) - 1;
            if (left == 0) {
              res.rules.add(r);
              res.index_rule(r);
              if (!seen_symbols.contains(r.lhs)) {
                seen_symbols.add(r.lhs);
                todo.add(r.lhs);
              }
            }

          }
        }
      } else
        foundStartsymbol = true;
    }
    if (!foundStartsymbol)
      throw new GrammarInitializationException();
    res.noadj_scores = noadj_scores;
    return res;
  }

  public void printGrammar() {
    for (Rule r : rules) {
      System.out.println(r);
    }
  }

}
