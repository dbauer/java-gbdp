package edu.columbia.nlp.gbdp.taggers;

import edu.columbia.nlp.util.averaged_perceptron.*;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Stream;

/**
 * Created by daniel on 7/23/16.
 */
public class Supertagger {

  AveragedPerceptron<NgramDataInstance> classifier;

  private static String[] split(String s){
    return s.split("\\s");
  }

  private AveragedPerceptronModel<NgramDataInstance> read_model_from_file(File model_file) throws IOException{
    AveragedPerceptronModel<NgramDataInstance> model;
    try {
      FileInputStream is = new FileInputStream(model_file);
      ObjectInputStream ois = new ObjectInputStream(is);
      model = (AveragedPerceptronModel<NgramDataInstance>) ois.readObject();
    } catch (ClassNotFoundException e) {
      e.printStackTrace();
      throw new IOException();
    }
    return model;
  }

  public Supertagger(File model_file) throws IOException {
    // Template that contains feature extraction lambdas
    FeatureSet<NgramDataInstance> template = SupertagFeatureExtractor.makeFeatureSet();

    AveragedPerceptronModel loaded_model = read_model_from_file(model_file);
    FeatureSet<NgramDataInstance> feature_definition = loaded_model.getFeatures();
    feature_definition.setExtractorsFromTemplate(template);
    classifier = new AveragedPerceptron(feature_definition, loaded_model.getWeights());
  }


  public List<Token[]> tagNBest(Token[] tokens, int n) {

    List<Token[]> ngrams = Token.getNgrams(7, Arrays.asList(tokens));

    List<List<ScoredLabel<String>>> stags = new ArrayList<>(tokens.length);
    int i = 0;
    for (Token[] ngram : ngrams) {
      if (ngram[3]!=null) {
        NgramDataInstance inst = new NgramDataInstance(null, ngram);
        List<ScoredLabel<String>> predicted = classifier.predictNbest(inst, n);
        stags.add(predicted);
      }
    }

    List<Token[]> result = new ArrayList<>();

    for (int j=0; j < tokens.length; j++) {
      ArrayList<Token> nbest = new ArrayList<Token>(n);
      for (ScoredLabel<String> tag : stags.get(j)) {
        nbest.add(new Token(tokens[j].word, tokens[j].pos, tag.getLabel(), tag.getScore()));
      }
      result.add(nbest.toArray(new Token[n]));
    }
    return result;
  }

  public Token[] tag(Token[] tokens) {

    List<Token[]> ngrams = Token.getNgrams(7, Arrays.asList(tokens));

    String[] stags = new String[tokens.length];
    int i = 0;
    for (Token[] ngram : ngrams) {
      if (ngram[6]!=null) {
        NgramDataInstance inst = new NgramDataInstance(null, ngram);
        String predicted = classifier.predict(inst);
        stags[i++] = predicted;
      }
    }

    Token[] result = new Token[tokens.length];
    for (int j=0; j< tokens.length; j++) {
      result[j] = new Token(tokens[j].word, tokens[j].pos, stags[j]);
    }
    return result;
  }

  private static String concatArray(String[] strings) {
    StringBuilder sb = new StringBuilder();
    for (int i = 0; i<strings.length; i++) {
      sb.append(strings[i]);
      sb.append(" ");
    }
    return sb.toString().trim();
  }



  public static List<Token[]> parseNbestSupertaggedLine(String line, int n) {
    List<Token[]> result = new ArrayList<>();
    for (String s : split(line)) {
      String[] tags_for_token = s.split("/");
      Token[] tokens = new Token[n];
      for (int i=0;i<n; i++) {
        String[] parts = tags_for_token[i].split(":");
        String tag = parts[0];
        double score = Math.log(Double.parseDouble(parts[1]));
        tokens[i] = new Token(null, null, tag, score);
      }
      result.add(tokens);
    }

    return result;
  }


  public static List<Token[]> parseNbestBeamSupertaggedLine(String line, int n, double ratio) {
    List<Token[]> result = new ArrayList<>();
    for (String s : split(line)) {
      double best_score = Double.NEGATIVE_INFINITY;
      String[] tags_for_token = s.split("/");
      List<Token> tokens = new ArrayList<Token>();
      boolean contin = true;
      int i = 0;
      while (contin && i<n) {
        String[] parts = tags_for_token[i].split(":");
        String tag = parts[0];
        double score = Math.log(Double.parseDouble(parts[1]));
        if (i==0) {
          best_score = score;
          tokens.add(new Token(null, null, tag, score));
        } else {
          if (score < best_score * ratio) {
            contin = false;
          } else {
            tokens.add(new Token(null, null, tag, score));
          }
        }
        i++;
      }
      result.add(tokens.toArray(new Token[tokens.size()]));
    }
    return result;
  }




  public static void main(String[] argv) throws IOException {

    File pos_model_file = new File(argv[0]);
    PosTagger postagger = new PosTagger(pos_model_file);
    System.err.println("POS tagger ready...");
    File stagger_model_file = new File(argv[1]);
    Supertagger stagger = new Supertagger(stagger_model_file);
    System.err.println("Supertagger ready...");

    String inputFile = argv[2];
    try (Stream<String> stream = Files.lines(Paths.get(inputFile))) {
      stream.forEach(new Consumer<String>() {
        @Override
        public void accept(String s) {
          String[] sentence = split(s);
          String[] pos_tags = postagger.tag(sentence);
          Token[] tokens = new Token[sentence.length];
          for(int i=0; i < sentence.length; i++)
            tokens[i] = new Token(sentence[i], pos_tags[i], null);

          List<Token[]> tagged_tokens= stagger.tagNBest(tokens,10);
          String[] result = new String[sentence.length];
          for (int i=0; i < sentence.length; i++) {
            Token[] nbest_for_word = tagged_tokens.get(i);

            String token_s = nbest_for_word[0].stag+":"+ Double.toString(nbest_for_word[0].getScore());
            for (int j = 1; j < nbest_for_word.length; j++) {
              if (nbest_for_word[j]!=null)
                token_s += "/" + nbest_for_word[j].stag+":"+ Double.toString(nbest_for_word[j].getScore());
              else
                token_s += "/";
            }
            result[i] = token_s;
          }
          System.out.println(concatArray(result));
        }
      });
    } catch (IOException e) {
      System.out.println(String.format("Error reading from input file %s", inputFile));
      e.printStackTrace();
      System.exit(1);
    }


  }


}
