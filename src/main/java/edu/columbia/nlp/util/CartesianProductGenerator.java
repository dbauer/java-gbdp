package edu.columbia.nlp.util;

import java.lang.UnsupportedOperationException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by bauer on 7/6/16.
 */
public class CartesianProductGenerator<T> implements Iterable<List<T>> {

  private ArrayList<ArrayList<T>> lists;
  private int combinations;

  public CartesianProductGenerator(ArrayList<ArrayList<T>> lists) {
    this.lists = lists;
    combinations = 1;
    for (ArrayList<T> l : lists) {
      this.combinations *= l.size();
    }

  }

  private class CartesianProductIterator implements Iterator<ArrayList<T>> {
    int currentCombination;

    public CartesianProductIterator() {
      currentCombination = 0;
    }

    public boolean hasNext() {
      return currentCombination < combinations;
    }

    public ArrayList<T> next() {
      ArrayList<T> result = new ArrayList<T>(lists.size());

      int i = 1;
      for (ArrayList<T> l : lists) {
        result.add(l.get((currentCombination / i) % l.size()));
        i *= l.size();
      }
      currentCombination++;
      return result;
    }

    public void remove() throws UnsupportedOperationException {
      throw new UnsupportedOperationException();
    }

  }

  public Iterator iterator() {
    return new CartesianProductIterator();
  }

}
