package edu.columbia.nlp.gbdp.parser;

import edu.columbia.nlp.gbdp.Chart;
import edu.columbia.nlp.gbdp.Edge;
import edu.columbia.nlp.gbdp.NBestBeamPQ;
import edu.columbia.nlp.gbdp.Traversal;
import edu.columbia.nlp.gbdp.exception.GrammarInitializationException;
import edu.columbia.nlp.gbdp.forest.Node;
import edu.columbia.nlp.gbdp.grammar.Grammar;
import edu.columbia.nlp.gbdp.grammar.NonterminalRuleToken;
import edu.columbia.nlp.gbdp.grammar.Rule;
import edu.columbia.nlp.gbdp.taggers.Supertagger;
import edu.columbia.nlp.gbdp.taggers.Token;
import edu.columbia.nlp.util.MinMaxHeap;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.Duration;
import java.util.*;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.function.Consumer;
import java.util.stream.Stream;

/**
 * Created by daniel on 6/29/16.
 */
public abstract class AbstractParser {
  static final boolean DEBUG =  false;

  protected Grammar grammar;
  protected Grammar sentence_grammar;
  protected Chart chart;
  private Properties config;

  List<Token[]> tokens;
  HashSet<Traversal> seen_traversals;
  HashSet<Edge> seen_edges;
  HashSet<Edge> finished_edges;
  MinMaxHeap<Traversal> exploration_agenda;
  MinMaxHeap<Edge> finishing_agenda;

  Map<Edge, Double> best_score;



  public AbstractParser(Grammar grammar, Properties config) throws IOException {
    this.grammar = grammar;
    this.config = config;
  }

  private static List<String> split(String s) {
    return Arrays.asList(s.split("\\s"));
  }
  public static List<Token[]> parseGoldStagLine(String line) {
    List<Token[]> result = new ArrayList<>();
    for (String s : split(line)) {
      Token[] tok = new Token[1];
      tok[0] = new Token(null, null, s);
      result.add(tok);
    }
    return result;
  }

  public static List<List<Token[]>> readStagedInput(InputStream input_file) throws FileNotFoundException, IOException {

    BufferedReader br = new BufferedReader(new InputStreamReader(input_file));
    String line;
    List<List<Token[]>> result = new LinkedList<>();
    while ((line = br.readLine()) != null) {
      result.add(parseGoldStagLine(line));
    }
    return result;
  }

  /**
   * Reset the parsing context to start a fresh parse.
   */
  protected void reset() {
    sentence_grammar = grammar.getSentenceGrammar(tokens);
    chart = new Chart(tokens.size()+1, sentence_grammar);
    exploration_agenda = new MinMaxHeap<>(); //NBestBeamPQ<>(10);// MinMaxHeap<>(); //RatioBeamPQ<>(1.01); //NBestBeamPQ<>(80);


    String finishing_agenda_beam_n_string = config.getProperty("finishingAgendaBeamN");
    if (finishing_agenda_beam_n_string == null) {
      finishing_agenda = new MinMaxHeap<>();
    } else {
      finishing_agenda = new NBestBeamPQ<>(Integer.parseInt(finishing_agenda_beam_n_string)); // NBestBeamPQ<>(10000);//MinMaxHeap<>(); //NBestBeamPQ<>(5000); //RatioBeamPQ<>(0.00008, 100); //MinMaxHeap<>(); //NBestBeamPQ<>(40);
    }

    seen_traversals = new HashSet<>();
    seen_edges = new HashSet<>();
    best_score = new HashMap<>();
    finished_edges = new HashSet<>();
  }


  abstract double scoreInitialEdge(Edge edge);

   Edge doShifts(Edge e) {
    return e.allShifts(tokens);
  }

  /**
   * Add initial edges to the finishing agenda.
   */
  protected void addInitialEdges() {
    if (DEBUG) System.out.println("===adding init edges ===");
    // add top-down rules that "prove" the start symbol
    for (Rule r : sentence_grammar.rules_for_lhs(grammar.startsymbol)) {
      Edge e = doShifts(new Edge(r, 0, 0, 0, 0.0));
      if (e!=null) {
        double score = scoreInitialEdge(e);
        e.setScore(score);
        e.setHeuristic(computeHeuristic(e));
        best_score.put(e, score);
        finishing_agenda.add(e);
        if (DEBUG) System.out.println(e);
      }
    }
  }

  abstract double scoreTraversal(Traversal t);

  private void makeCompleteTraversal(Edge a, Edge p) {
    Edge new_edge;
    if (p == null) {
      new_edge = doShifts(a.skip());
    } else {
      new_edge = a.complete(p);
      new_edge = doShifts(new_edge);

    }
    if (new_edge != null) {
      // compute the score for this traversal
      Traversal t = new Traversal(a, p, new_edge, 0.0);
      //double traversal_score = scoreTraversal(t);


      double total_score =  scoreTraversal(t); //traversal_score + best_score.get(a) + best_score.get(p) ;
      new_edge.setScore(total_score);
      new_edge.setHeuristic(computeHeuristic(new_edge));
      t.setScore(total_score);
      if (!seen_traversals.contains(t)) {
        if (DEBUG) System.out.println("  Adding traversal " + t);
        exploration_agenda.add(t);
      }
    }
  }

  private void doCompletes(Edge e) {
    if (e.isActive()) {
      // active edge
      // Check if e can be completed with a passive edge
      NonterminalRuleToken outside = (NonterminalRuleToken) e.getOutside();
      for (Edge p : chart.getPassiveWithStart(e.getRight(), outside.value))
        makeCompleteTraversal(e,p);
    } else {
      // passive edge
      // Check if e can complete an active edge
      for (Edge a : chart.getActiveWithEnd(e.getLeft(), e.getRule().lhs)) {
        if (DEBUG) System.out.println(" found active edge "+a);
        makeCompleteTraversal(a, e);
      }
    }
  }

  private void updateScore(Edge e) {
    double score = e.getScore();
    if (best_score.containsKey(e)) {
      if (best_score.get(e) < score) {
        best_score.put(e, score);
      }
    } else
      best_score.put(e, score);
  }

  private void doPredicts(Edge a) {
    for (Rule rule : sentence_grammar.rules_for_lhs(a.getOutside().value)) {
      if (rule.terminal_count + rule.nonterminal_count + a.getRight() <= tokens.size()) {
        Edge new_edge;
        new_edge = new Edge(rule, a.getRight(), a.getRight(), 0, 0.0);
        new_edge = doShifts(new_edge);


        if (new_edge != null) {
          new_edge.setScore(scoreInitialEdge(new_edge));
          new_edge.setHeuristic(computeHeuristic(new_edge));
          updateScore(new_edge);
          if (DEBUG) System.out.println("adding " + new_edge.toString());
          if (!seen_edges.contains(new_edge)) {
            seen_edges.add(new_edge);
            finishing_agenda.add(new_edge);
          }
        }
      }
    }
  }

  protected void finishEdge(Edge e) {
    //System.out.println(e+" "+e.getScore() + " " +e.getHeuristic() +" "+ e.getSum());
    if (DEBUG) System.out.println("Finishing Edge "+e);
    if (!finished_edges.contains(e)) {
      // This can happen with limited beam search, but shouldn't cause any problems.
      //if (e.getScore() != best_score.get(e)) {
      //  System.err.println("finishing edge "+e+ " but best_score map contains better score "+ best_score.get(e));
      //  System.exit(1);
      //}
      finished_edges.add(e);
      e.setScore(best_score.get(e)); // This is a hack solving the problem that we may have
      //e.setHeuristic(computeHeuristic(e)); NOT NEEDED BECAUSE SPAN HASN'T CHANGED
      // previously found a better way to build this edge, but that traversal fell
      // out of the beam.
      chart.addEdge(e); // This edge now has the maximum score
      doCompletes(e);
      if (e.isActive()) {
        doPredicts(e);
        doNoAdj(e);
      }
    }
  }

  abstract double scoreNoAdjunction(Traversal t);

  private void doNoAdj(Edge a) {
    NonterminalRuleToken outside = (NonterminalRuleToken) a.getOutside();
    if (outside.hasKleene) {

      // TODO: Maybe store traversals only according to their individual score, NOT the viterbi score for the result edge.
      //   Do the traversals need to be on a PQ at all (since there is no beam on them?)
      Edge new_edge1 = new Edge(a.getRule(), a.getLeft(), a.getRight(), a.getDot() + 1, 0.0);
      Edge new_edge = doShifts(new_edge1);
      if (new_edge != null) {
        Traversal new_traversal = new Traversal(a, null, new_edge, 0.0);

        double no_adj_score = scoreNoAdjunction(new_traversal);
        new_traversal.setScore(no_adj_score);
        new_edge.setScore(no_adj_score);
        new_edge.setHeuristic(computeHeuristic(new_edge));

        exploration_agenda.add(new_traversal);
        if (DEBUG) System.out.println("  Possible null adjunction" + new_edge);
      }
    }
  }

  double computeHeuristic(Edge e) {
    return 0.0;
  }


  protected void exploreTraversal(Traversal t) {

    // The score for this traversal should not have changed, otherwise we
    // would get another traversal first.
    if (!seen_traversals.contains(t)) {
      seen_traversals.add(t); // this happens even if the result edge has been visited aleady
      chart.addTraversal(t);
      Edge newEdge = t.getResult();
      updateScore(newEdge); // see if viterbi score or newEdge can be updated
      //if (DEBUG) System.out.println("Adding edge "+newEdge+" to finishing agenda.");
      finishing_agenda.add(newEdge);
    }
  }

  public Node parse(List<Token[]> tokens) {
    this.tokens = tokens;
    try {
      reset();
    } catch (GrammarInitializationException e) {
      //e.printStackTrace();
      //System.err.println("Grammar cannot parse sentence.");
      return null;
    }
    addInitialEdges();

    // Main parser loop
    while (!finishing_agenda.empty()) {

      finishEdge(finishing_agenda.pollMax());
      while (!exploration_agenda.empty())
        exploreTraversal(exploration_agenda.pollMax());
    }
    if (DEBUG) System.out.println("max entries in exploration agenda "+exploration_agenda.max_beam_size);
    if (DEBUG) System.out.println("max entries in finishing agenda "+finishing_agenda.max_beam_size);
    return chart.get_one_best();
  }




}
