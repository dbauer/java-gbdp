package edu.columbia.nlp.gbdp;

import edu.columbia.nlp.gbdp.exception.OutOfBeamException;
import edu.columbia.nlp.util.MinMaxHeap;

/**
 * Created by daniel on 7/1/16.
 */
public class NBestBeamPQ<T extends Comparable<? super T>> extends MinMaxHeap<T> {

  int beam_width;

  public NBestBeamPQ(int beam_width) {
    super(beam_width);
    this.beam_width = beam_width;
  }

  @Override
  public void add(T t) throws OutOfBeamException {
    if (size() < beam_width) {
      super.add(t);
    } else {
      // Too many elements in the heap.
      // If t is larger than the min in the heap, replace min with t.
      // Otherwise ignore t.
      if (t.compareTo(getMin()) > 0) {
        pollMin();
        super.add(t);
      } //else {
        //throw new OutOfBeamException();
      //}
    }
  }

}
