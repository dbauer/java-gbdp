package edu.columbia.nlp.util.averaged_perceptron;

/**
 * Created by daniel on 7/20/16.
 */
public interface DataInstance {

  public String getLabel();

  public String getDescription();

}
