package edu.columbia.nlp.util.averaged_perceptron;

import edu.columbia.nlp.gbdp.NBestBeamPQ;
import edu.columbia.nlp.gbdp.taggers.ScoredLabel;
import edu.columbia.nlp.util.MinMaxHeap;

import java.util.*;

/**
 * Created by daniel on 7/15/16.
 */
public class AveragedPerceptron<T extends DataInstance > {

  private final static boolean DEBUG = true;
  public final static double LEARNING_RATE = 1.0;

  private FeatureSet<T> features;
  public Map<String, SparseVector> weights;
  private Map<String, SparseVector> weightsum;
  private Map<String, Map<Integer, Integer>> last_update_time;

  private Map<String, SparseVector> normalized_weights;
  private boolean has_changed;
  private double negativeScoreOffset;

  private int current_time = 0;

  public AveragedPerceptron(){
    weights = new HashMap<>();
    weightsum = new HashMap<>();
    last_update_time = new HashMap<>();
    features = new FeatureSet<T>();
    this.normalized_weights = new HashMap<>();
    this.has_changed = true;
    negativeScoreOffset =  -2^5;
  }

  public AveragedPerceptron(FeatureSet<T> features, Map<String, SparseVector> weights){
    this();
    this.features = features;
    this.weights = weights;
  }

  public AveragedPerceptron(FeatureSet<T> features) {
    this();
    this.features = features;
  }


  public AveragedPerceptron(AveragedPerceptronModel<T> model) {
    this(model.getFeatures(), model.getWeights());
  }

  public double score(String label, ClassificationInstance instance) {
    return instance.getFeatureValues().dotProduct(weights.get(label));
  }

  public double scoreAllNegative(String label, T instance) {
    SparseVector feats = features.apply(instance);
    return scoreAllNegative(label, feats);

  }

  /**
   * This method is broken! Don't use.
   * @param label
   * @param v
   * @return
   */
  public double scoreAllNegative(String label, SparseVector v) {
    //if (has_changed) {
    //  normalized_weights.put(label, weights.get(label).toUnitVector());
    //  has_changed = false;
    //}
    double score = score(label, v);
    return(score(label, v) - negativeScoreOffset);
  }

  public double score(String label, SparseVector v) {
    return v.dotProduct(weights.get(label));
  }

  public double score(String label, T instance) {
      SparseVector feats = features.apply(instance);
      return score(label, feats);
  }

  public String predict(ClassificationInstance instance) {
    String max_label = null;
    double max_weight = Double.NEGATIVE_INFINITY;

    for (String label: weights.keySet()) {
      double weight = score(label, instance);
      if (weight > max_weight) {
        max_label = label;
        max_weight = weight;
      }
    }
    return max_label;
  }

  public List<ScoredLabel<String>> predictNbest(ClassificationInstance instance, int n) {
    String max_label = null;
    double max_weight = Double.NEGATIVE_INFINITY;

    Map<String, Double> label_to_score = new HashMap<>();

    MinMaxHeap<ScoredLabel<String>> pq = new MinMaxHeap<>();

    double sum = 0.0;
    for (String label: weights.keySet()) {
      double weight = instance.getFeatureValues().dotProduct(weights.get(label));
      sum += weight;
      label_to_score.put(label, weight);
    }

    for (String label : label_to_score.keySet()) {
      double new_weight = label_to_score.get(label)/sum;
      pq.add(new ScoredLabel<String>(label, new_weight));
    }

    List<ScoredLabel<String>> result = new ArrayList<>();
    for (int i=0; i < n; i++) {
      result.add(pq.pollMax());
    }

    return result;
  }

  public void initLabel(String label) {
    if (!weights.containsKey(label)) {
      weights.put(label, new SparseVector());
      weightsum.put(label, new SparseVector());
      last_update_time.put(label, new HashMap<>());
      has_changed = true;
    }
  }

  public void updateWeights(String label, SparseVector features, Double update) {
    SparseVector weight_v = weights.get(label);

    for (Integer i : features.keySet()) {
      Double current_val = weight_v.get(i);
      if (current_val == null) current_val = 0.0;

      // update running totals for averaging
      Integer last_update_this_feature = last_update_time.get(label).get(i);
      if (last_update_this_feature != null) {
        SparseVector total_v = weightsum.get(label);
        Double current_total = total_v.get(i);
        if (current_total == null) current_total = 0.0;
        int passed_iterations = current_time - last_update_this_feature;
        total_v.put(i, current_total + passed_iterations * current_val);
      }

      weight_v.put(i, current_val + update);
      last_update_time.get(label).put(i, current_time);
    }
    has_changed = true;
  }

  public void updateWeights(SparseVector features, Double update) {
    updateWeights("", features, update);
  }

  public void normalize() {
    for (String label : weights.keySet()) {
      double sum = 0.0;
      SparseVector features = weights.get(label);
      for (double value : features.values())
        sum += value;
      for (Integer f : features.keySet()) {
        features.put(f, features.get(f)/sum);
      }

    }
  }

  public void training_step(ClassificationInstance instance) {
    String true_label = instance.getLabel();
    initLabel(true_label);
    String predicted = predict(instance);

    initLabel(predicted);
    if (!weightsum.containsKey(true_label)) {
      weightsum.put(true_label, new SparseVector());
      last_update_time.put(true_label, new HashMap<>(SparseVector.DEFAULT_CAPACITY));
    }
    if (!weightsum.containsKey(predicted)) {
      weightsum.put(predicted, new SparseVector());
      last_update_time.put(predicted, new HashMap<>(SparseVector.DEFAULT_CAPACITY));
    }

    if (!predicted.equals(true_label)) {
      updateWeights(true_label, instance.getFeatureValues(), LEARNING_RATE);
      updateWeights(predicted, instance.getFeatureValues(), -LEARNING_RATE);
    } else {
      //System.out.println("correct!");
    }
    current_time ++;
  }

  public void training_iteration(Collection<T> instances, FeatureSet<T> features) {

    int number_correct = 0;
    int total = 0;
    //System.out.println("===========");

    for (T inst : instances) {
      if (total % 10000 == 0) {
        System.err.print(".");
      }
      ClassificationInstance instance = features.makeClassificationInstance(inst);
      String true_label = instance.getLabel();
      initLabel(true_label);
      String predicted = predict(instance);

      initLabel(predicted);
      if (!weightsum.containsKey(true_label)) {
        weightsum.put(true_label, new SparseVector());
        last_update_time.put(true_label, new HashMap<>(997));
      }
      if (!weightsum.containsKey(predicted)) {
        weightsum.put(predicted, new SparseVector());
        last_update_time.put(predicted, new HashMap<>(997));
      }

      if (!predicted.equals(true_label)) {
        updateWeights(true_label, instance.getFeatureValues(), LEARNING_RATE);
        updateWeights(predicted, instance.getFeatureValues(), -LEARNING_RATE);
      } else {
        //System.out.println("correct!");
        number_correct += 1;
      }
      total++;
      current_time ++;
      //System.err.println("predicting " +predicted);
      //System.err.println(weights);

    }
    System.err.println(".");
    if (DEBUG) System.err.println("Acc: "+number_correct +"/"+total +" = "+  number_correct/ (float) total);
    has_changed = true;
  }

  public void average_weights() {

    // do one last update, then average
    for (String label : last_update_time.keySet()) {
      SparseVector total_v = weightsum.get(label);
      if (total_v == null) total_v = new SparseVector();
      weightsum.put(label, total_v);

      for (Integer i : last_update_time.get(label).keySet()) {
        Double current_weight = weights.get(label).get(i);
        Double current_total = total_v.get(i);
        if (current_total == null) current_total = 0.0;
        Integer last_update_this_feature = last_update_time.get(label).get(i);
        int passed_iterations = current_time - last_update_this_feature;
        Double final_weight = current_total + passed_iterations * current_weight;
        final_weight = final_weight / current_time;
        total_v.put(i, final_weight);
      }
    }
    has_changed = true;
  }

  public  Map<String,SparseVector> train(List<T> instances, FeatureSet<T> features, int iterations) {
    for (int current_iteration = 0; current_iteration < iterations; current_iteration++) {
      Collections.shuffle(instances);
      System.err.println("Starting iteration "+ current_iteration);
      training_iteration(instances, features);
    }
    average_weights();
    weights = weightsum;
    return weights;
  }

/*
  private static ClassificationInstance parseLine(String l){

    String[] main_and_comment = l.trim().split("#");
    String line = main_and_comment[0];

    Map<Integer, Float> features = new HashMap<>();
    String[] fields = line.split("\\s");
    String label = fields[0];
    for (int i=1; i<fields.length; i++){
      String[] parts = fields[i].split(":");
      Integer feature = Integer.parseInt(parts[0]);
      Float value = Float.parseFloat(parts[1]);
      features.put(feature, value);
    }
    return new ClassificationInstance(label, features);
  }

  public static List<ClassificationInstance> read_svm_light_data(String input_file) throws IOException {

    Stream<String> stream = Files.lines(Paths.get(input_file));
    Stream<ClassificationInstance> sentence_stream = stream.map(x->parseLine(x));

    return sentence_stream.collect(Collectors.toList());

  }
*/

  public String predict(T instance) {
    SparseVector v = features.apply(instance);

    return predict(new ClassificationInstance(instance.getLabel(), features.apply(instance)));
  }

  public List<ScoredLabel<String>> predictNbest(T instance, int n) {
    SparseVector v = features.apply(instance);

    return predictNbest(new ClassificationInstance(instance.getLabel(), features.apply(instance)), n);
  }

  public double evaluate_data(Collection<ClassificationInstance> data) {
    int total = 0;
    int correct = 0;
    for (ClassificationInstance instance : data) {
      String predict = predict(instance);
      if (predict.equals(instance.getLabel()))
        correct++;
      else {
        //System.out.println("Error. Predicted "+predict+" for "+instance.getDescription());
      }
      total++;
    }
    double acc = correct / (double) total;
    if (DEBUG) System.err.println("Acc: "+correct+"/"+total+" = "+acc);
    return acc;
  }


/*
  public static void main(String[] argv) throws IOException{

    List<ClassificationInstance> data = read_svm_light_data(argv[0]);
    AveragedPerceptron ap = new AveragedPerceptron();
    System.out.println("Training...");
    Map<String, Map<Integer,Float>> model = ap.train(data,1000);

  }
*/

}
