package edu.columbia.nlp.util;

/**
 * Created by daniel on 7/1/16.
 * A doubly ended priority
 */
public interface DoubleEndedPriorityQueue<T extends Comparable<?super T>> {
  public T pollMax();
  public T pollMin();
  public T getMin();
  public T getMax();
  public void add(T t);
  public int size();
  public boolean empty();
}
