package edu.columbia.nlp.util.averaged_perceptron;

import java.io.Serializable;
import java.util.HashMap;
import java.util.function.Function;

/**
 * Created by daniel on 7/20/16.
 */
public class OneHotFeature<S,T> implements Serializable {

  private static final long serialVersionUID = 4676751204566296955L;

  private transient Function<S,T> extractor;


  /**
   * @serial
   */
  private HashMap<T, Integer> concrete_feature_map;
  private int capacity;


  public OneHotFeature(Function<S,T> extractor, int capacity) {
    this.extractor = extractor;
    this.capacity = capacity;
    concrete_feature_map = new HashMap<>(SparseVector.DEFAULT_CAPACITY);
  }

  public OneHotFeature(Function<S,T> extractor, HashMap<T, Integer> concrete_feature_map, int capacity) {
    this(extractor, capacity);
    setConcreteFeatureMap(concrete_feature_map);
  }

  public void setConcreteFeatureMap(HashMap<T, Integer> concrete_feature_map) {
    this.concrete_feature_map = concrete_feature_map;
    if (concrete_feature_map.size() > capacity)
      throw new FeatureDimensionsExceededError();
  }

  public OneHotFeature<S,T> spawn(Function<S,T> extractor) {
    return new OneHotFeature<S,T>(extractor, concrete_feature_map, capacity);
  }

  public int getCapacity() {
    return capacity;
  }

  public int getLength(){
    return concrete_feature_map.size();
  }

  public SparseVector apply(S input) {
    return applyWithOffset(input, 0);
  }

  public SparseVector applyWithOffset(S input, int offset) {
    SparseVector result = new SparseVector();
    T concrete_feature = extractor.apply(input);

    if (concrete_feature == null)
      return result;

    Integer index = concrete_feature_map.get(concrete_feature);
    if (index == null) {
      int new_index = concrete_feature_map.size();
      if (new_index >= capacity)
        throw new FeatureDimensionsExceededError(concrete_feature.toString()+" "+String.valueOf(capacity));
      concrete_feature_map.put(concrete_feature, new_index);
      result.put(new_index + offset, 1.0);
    } else {
      result.put(index + offset, 1.0);
    }
    return result;
  }

  public void initialize(S input) {
    apply(input);
    /*T concrete_feature = extractor.apply(input);
    Integer index = concrete_feature_map.get(concrete_feature);
    if (index == null) {
      index = concrete_feature_map.size();
      concrete_feature_map.put(concrete_feature, index);
    }*/
  }

  public Function<S,T> getExtractor(){
    return extractor;
  }

  public void setExtractor(Function<S,T> extractor){
    this.extractor = extractor;
  }

}