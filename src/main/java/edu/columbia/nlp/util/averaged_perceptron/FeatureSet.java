package edu.columbia.nlp.util.averaged_perceptron;

import java.io.Serializable;
import java.util.*;
import java.util.function.Predicate;
import java.util.function.Function;

/**
 * Created by daniel on 7/20/16.
 */
public class FeatureSet<T extends DataInstance> implements Serializable {

  private static final long serialVersionUID = -3215937459473847722L;

  /**
   * @serial
   */
  public HashMap<String, OneHotFeature<T, String>> vector_features;
  public HashMap<String, MultipleHotFeature<T, String>> multihot_features;
  public transient HashMap<String, Predicate<T>> binary_features;
  public transient HashMap<String, Function<T, Double>> numeric_features;
  private ArrayList<String> vector_feature_key_list;
  private ArrayList<String> binary_feature_key_list;
  private ArrayList<String> numeric_feature_key_list;
  private ArrayList<String> multihot_feature_key_list;

  public FeatureSet() {
    vector_features = new HashMap<>();
    binary_features = new HashMap<>();
    numeric_features = new HashMap<>();
    multihot_features= new HashMap<>();
  }

  public void addVectorFeature(String name, OneHotFeature<T, String> feature) {
    vector_features.put(name, feature);
  }

  public void addBinaryFeature(String name, Predicate<T> feature) {
    binary_features.put(name, feature);
  }

  public void addNumericFeature(String name, Function<T,Double> feature) {numeric_features.put(name, feature);}

  public void addMultiHotFeature(String name, MultipleHotFeature<T,String> feature) {multihot_features.put(name, feature);}

  public SparseVector apply(T input) { return applyWithOffset(input, 0); }

  public List<ClassificationInstance> getClassificationInstances(Iterable<T> instances) {
    List<ClassificationInstance> result = new ArrayList<>(1000000);
    int i = 0;
    for (T instance : instances) {
      if (i%10000 == 0) {
        System.out.println(i);
        Runtime.getRuntime().gc();
      }
      result.add(new ClassificationInstance(instance.getLabel(), apply(instance), instance.getDescription()));
      i++;
    }
    return result;
  }

  public ClassificationInstance makeClassificationInstance(T instance) {
     return new ClassificationInstance(instance.getLabel(), apply(instance), instance.getDescription());
  }

  public SparseVector applyWithOffset(T input, int offs) {

    SparseVector result = new SparseVector();
    int offset = offs;

    if (multihot_features == null)
      multihot_features = new HashMap<>();

    if (vector_feature_key_list == null) {
      ArrayList<String> keys = new ArrayList<>(vector_features.keySet());
      keys.sort(new Comparator<String>() {
        public int compare(String a, String b) {
          return a.compareTo(b);
        }
      });
      vector_feature_key_list = keys;
    }

    if (binary_feature_key_list == null) {
      ArrayList<String> keys = new ArrayList<>(binary_features.keySet());
      keys.sort(new Comparator<String>() {
        public int compare(String a, String b) {
          return a.compareTo(b);
        }
      });
      binary_feature_key_list = keys;
    }

    if (numeric_feature_key_list == null) {
      ArrayList<String> keys = new ArrayList<>(numeric_features.keySet());
      keys.sort(new Comparator<String>() {
        public int compare(String a, String b) {
          return a.compareTo(b);
        }
      });
      numeric_feature_key_list = keys;
    }

    if (multihot_feature_key_list == null) {
      ArrayList<String> keys = new ArrayList<>(multihot_features.keySet());
      keys.sort(new Comparator<String>() {
        public int compare(String a, String b) {
          return a.compareTo(b);
        }
      });
      multihot_feature_key_list = keys;
    }

    for (String key : vector_feature_key_list) {
      OneHotFeature<T, String> feature_type = vector_features.get(key) ;
      SparseVector vector_for_feature =  feature_type.applyWithOffset(input, offset);
      result.putAll(vector_for_feature);
      offset += feature_type.getCapacity();
    }

    for (String key : multihot_feature_key_list) {
      MultipleHotFeature<T, String> feature_type = multihot_features.get(key) ;
      SparseVector vector_for_feature =  feature_type.applyWithOffset(input, offset);
      result.putAll(vector_for_feature);
      offset += feature_type.getCapacity();
    }

    for (String key : binary_feature_key_list) {
      Predicate<T> feature = binary_features.get(key);
      if (feature.test(input))
        result.put(offset++, 1.0);
    }
    for (String key : numeric_feature_key_list) {
      Function<T, Double> feature = numeric_features.get(key);
      result.put(offset++, feature.apply(input));
    }

    return result;
  }

  public int getLength() {
    return vector_features.values().stream().map(x->x.getCapacity()).reduce(0, Integer::sum) +
            multihot_features.values().stream().map(x->x.getCapacity()).reduce(0, Integer::sum) +
            binary_features.size() +numeric_features.size();
  }

  public void initialize(T input) {
    vector_features.values().forEach(x->x.initialize(input));
  }

  public void initialize(Iterable<T> input) {
    int i = 0;
    for (T inst : input) {
      if (i%1000 == 0) {
        System.out.println(i);
      }
      initialize(inst);
      i++;
    }
  }

  public HashMap<String, OneHotFeature<T, String>> getVectorFeatures(){
    return vector_features;
  }

  public HashMap<String, MultipleHotFeature<T, String>> getMultihotFeatures(){ return multihot_features;}

  public HashMap<String, Predicate<T>> getBinaryFeatures(){
    return binary_features;
  }

  public HashMap<String, Function<T, Double>> getNumericFeatures(){
    return numeric_features;
  }


  public void setExtractorsFromTemplate(FeatureSet<T> template) {




    HashMap<String, OneHotFeature<T,String>> features = template.getVectorFeatures();
    for (String name : features.keySet()) {
      OneHotFeature<T,String> feature = features.get(name);
      vector_features.get(name).setExtractor(feature.getExtractor());
    }

    HashMap<String, MultipleHotFeature<T,String>> features2 = template.getMultihotFeatures();
    //this.multihot_features = new HashMap<String, MultipleHotFeature<T, String>>();
    for (String name : features2.keySet()) {
      MultipleHotFeature<T,String> feature = features2.get(name);
      multihot_features.get(name).setExtractor(feature.getExtractor());
    }

    binary_features = template.getBinaryFeatures();
    numeric_features = template.getNumericFeatures();
  }

}
