package edu.columbia.nlp.util.averaged_perceptron;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

/**
 * Created by daniel on 7/20/16.
 */
public class SparseVector extends HashMap<Integer, Double> implements Serializable {

  private static final long serialVersionUID = -3124517186301648023L;

  public static final int DEFAULT_CAPACITY = 31;

  public SparseVector() {
    super(DEFAULT_CAPACITY);
  }


  public double dotProduct(SparseVector other) {
    double sum = 0.0;
    if (size() > other.size()) {
      for (Map.Entry<Integer, Double> entry : other.entrySet()) {
        Double thisi = get(entry.getKey());
        if (thisi!= null) sum += (thisi * entry.getValue());
      }
    } else {
      for (Map.Entry<Integer,Double> entry : entrySet()) {
        Double otheri = other.get(entry.getKey());
        if (otheri != null) sum += (otheri * entry.getValue());
      }
    }
    return sum;
  }

  public SparseVector toUnitVector() {
    double sum = 0;
    for (Integer i : keySet()) {
      sum += get(i);
    }
    double norm = Math.sqrt(sum);

    SparseVector result = new SparseVector();
    for (Integer i : keySet()) {
      result.put(i, get(i) / norm);
    }
    return result;
  }

}
