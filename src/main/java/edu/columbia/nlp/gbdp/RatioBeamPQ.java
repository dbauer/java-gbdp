package edu.columbia.nlp.gbdp;

import edu.columbia.nlp.gbdp.exception.OutOfBeamException;
import edu.columbia.nlp.util.MinMaxHeap;

/**
 * Created by daniel on 7/1/16.
 */
public class RatioBeamPQ <T extends HasScore> extends MinMaxHeap<T> {

  double ratio;
  int n;

  public RatioBeamPQ(double ratio, int n) {
    super();
    this.ratio = ratio;
    this.n = n;
  }

  private void prune(int n) {

    double tscore = getMax().getScore();
    while (size() > n && (tscore + Math.log(ratio)) > getMin().getScore()) {
      //System.out.println("   removed "+pollMin());
      pollMin();
    }

  }

  @Override
  public void add(T t) {

    //if (empty()) {
    super.add(t);
    prune(n);
   //} else {
   //   T current_max = getMax();


  //    double tscore = t.getScore();
  //    if (tscore > current_max.getScore()) {
  //      System.out.println(" Added.");
  //      super.add(t);
  //      System.out.println("keep items > "+ (tscore + Math.log(ratio)));
    /*
        prune(n);
        return;
      }
      if (tscore > (current_max.getScore() + Math.log(ratio))) {
        super.add(t);
        System.out.println(" Added.");
       } else {
        System.out.println(" not added");
      }

      prune(n);
      //else throw new OutOfBeamException();
    }*/
  }

}
