package edu.columbia.nlp.gbdp.taggers;

import edu.columbia.nlp.util.averaged_perceptron.*;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by daniel on 7/21/16.
 */
public class SupertagFeatureExtractor {

  public static FeatureSet<NgramDataInstance> makeFeatureSet() {

    FeatureSet<NgramDataInstance> features = new FeatureSet<>();

    OneHotFeature<NgramDataInstance, String> postag = new OneHotFeature<>(x->x.getPos(3), 60);
    features.addVectorFeature("postag", postag);
    features.addVectorFeature("postagm1", postag.spawn(x->x.getPos(2)));
    features.addVectorFeature("postagm2", postag.spawn(x->x.getPos(1)));
    features.addVectorFeature("postagm3", postag.spawn(x->x.getPos(0)));
    features.addVectorFeature("postagp1", postag.spawn(x->x.getPos(4)));
    features.addVectorFeature("postagp2", postag.spawn(x->x.getPos(5)));
    features.addVectorFeature("postagp3", postag.spawn(x->x.getPos(6)));

    OneHotFeature<NgramDataInstance, String> postagtriple = new OneHotFeature<>(x-> x.getPos(2)+"_"+x.getPos(3)+"_"+x.getPos(4), 60*60*60);
    features.addVectorFeature("postagtriple", postagtriple);

    //OneHotFeature<NgramDataInstance, String> postagquintruple = new OneHotFeature<>(x-> x.getPos(1) + "_" +x.getPos(2)+"_"+x.getPos(3)+"_"+x.getPos(4)+"_"+x.getPos(5), 60*60*60*60*60);
    //features.addVectorFeature("postagquintruple", postagquintruple);

    //OneHotFeature<NgramDataInstance, String> postagtripleword = new OneHotFeature<>(x-> x.getPos(2)+"_"+x.getWord(3)+"_"+x.getPos(4), 60*60000*60);
    //features.addVectorFeature("postagtripleword", postagtripleword);

    OneHotFeature<NgramDataInstance, String> suffix3 = new OneHotFeature<NgramDataInstance, String>(x->x.getSuffix(3,3), 40 * 40 * 40);
    features.addVectorFeature("suffix3", suffix3);
    //features.addVectorFeature("suffix3m1", suffix3.spawn(x->x.getSuffix(2,3)));
    //features.addVectorFeature("suffix3m2", suffix3.spawn(x->x.getSuffix(1,3)));
    //features.addVectorFeature("suffix3p1", suffix3.spawn(x->x.getSuffix(4,3)));
    //features.addVectorFeature("suffix3p2", suffix3.spawn(x->x.getSuffix(5,3)));


    Predicate<NgramDataInstance> containsDash = (x -> x.containsDash(3));
    features.addBinaryFeature("containsDash", containsDash);

    Predicate<NgramDataInstance> isNumber = (x -> x.isNumber(3));
    features.addBinaryFeature("thisIsNumber", isNumber);

    OneHotFeature<NgramDataInstance, String> thisword = new OneHotFeature<NgramDataInstance, String>(x->x.getWord(3), 60000);
    features.addVectorFeature("thisword", thisword);

    //features.addVectorFeature("wordm1", thisword.spawn(x->x.getWord(2)));
    //features.addVectorFeature("wordm2", thisword.spawn(x->x.getWord(1)));
    //features.addVectorFeature("wordm3", thisword.spawn(x->x.getWord(0)));
    //features.addVectorFeature("wordp1", thisword.spawn(x->x.getWord(4)));
    //features.addVectorFeature("wordp2", thisword.spawn(x->x.getWord(5)));
    //features.addVectorFeature("wordp3", thisword.spawn(x->x.getWord(6)));

    return features;
  }



    public static List<NgramDataInstance> readTrainingData(String inputFile) throws IOException{
      return readTrainingData(inputFile, null);
    }

    public static List<NgramDataInstance> readTrainingData(String inputFile, PosTagger postagger) throws IOException {

    Stream<String> stream = Files.lines(Paths.get(inputFile));
    Stream<List<Token>> sentence_stream = stream.map(x->Token.parseLine(x));
    List<NgramDataInstance> instances = new ArrayList<>();

    sentence_stream.forEach(new Consumer<List<Token>>() {
      public void accept(List<Token> sentence) {

        List<Token> input = sentence;
        // If a postagger instance was passed on, use it to POS tag the input.
        // Otherwise gold tags are used.
        if (postagger != null) {
          input = postagger.tag(sentence);
        }

        for (Token[] ngram : Token.getNgrams(7, input)) {
          if (ngram[3]!=null) {
            NgramDataInstance instance = new NgramDataInstance(ngram[3].stag, ngram);
            instances.add(instance);
          }
        }
      }
    });
    return instances;
  }

  public static List<NgramDataInstance> filterTags(List<NgramDataInstance> data) {

    HashMap<String, Integer> counts = new HashMap<>();

    for (NgramDataInstance instance : data) {
      if (!counts.containsKey(instance.label))
        counts.put(instance.label, 1);
      else
        counts.put(instance.label, counts.get(instance.label) +1);
    }

    ArrayList<NgramDataInstance> result = new ArrayList<>(data.size());

    return data.stream().filter(x -> counts.get(x.label) > 1 ).collect(Collectors.toList());
  }

  public static void main(String[] argv) throws IOException {
    System.err.println("Extracting features...");
    SupertagFeatureExtractor extractor = new SupertagFeatureExtractor();

    //

    File model_file = new File(argv[0]);
    PosTagger tagger = new PosTagger(model_file);

    List<NgramDataInstance> data = filterTags(readTrainingData(argv[1], tagger));

    System.out.println(data.size()+" filtered instances.");

    System.out.println("creating feature set...");
    FeatureSet<NgramDataInstance> features = SupertagFeatureExtractor.makeFeatureSet();
    //features.initialize(data);



    System.err.println("Training averaged perceptron...");
    AveragedPerceptron<NgramDataInstance> ap = new AveragedPerceptron<>();
    System.out.println("Training...");
    Map<String, SparseVector> values = ap.train(data, features, 10);

    System.err.println("Evaluating...");
    List<NgramDataInstance> data2 = readTrainingData(argv[2]);
    List<ClassificationInstance> evalInstances = features.getClassificationInstances(data2);
    ap.evaluate_data(evalInstances);

    FileOutputStream fos = new FileOutputStream("supertagger_model.dat");
    ObjectOutputStream oos = new ObjectOutputStream(fos);

    AveragedPerceptronModel<NgramDataInstance> model = new AveragedPerceptronModel<>(features, values);
    oos.writeObject(model);
    oos.close();
    System.out.println("saved model to supertagger_model.dat");
  }
}
