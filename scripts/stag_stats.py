import sys
from collections import defaultdict

def get_nbest_tags(predict_line):
    tokens = predict_line.split(" ")
    result = []
    for t in tokens:
        parts = t.split("/")
        nbest = [(tuple(x.split(":"))) for x in parts]
        nbest = [(tag, float(score_s)) for tag,score_s in nbest]
        result.append(nbest)
    return result


#seen_tags_at_i = {}

#for i in range(10): 
#    seen_tags_at_i[i] = set()


seen_tags = set()

ratio = float(sys.argv[2])

with open(sys.argv[1]) as predict_file: 
    tags_for_word = []
    for predict_line in predict_file: 
        predict_tags = get_nbest_tags(predict_line)

        for tags in predict_tags:
            best_tag, best_score = tags[0]
            seen_tags.add(best_tag)
            ntags = 1
            for i in range(1,9): 
                tag, score = tags[i] 
                #seen_tags_at_i[i].update(set([tag for (tag,score) in tags[:i+1]]))
                if score < ratio * best_score: 
                   break 
                seen_tags.add(tag)
                ntags+=1
            tags_for_word.append(ntags)
    #for i in range(10): 
    #    print i+1, len(seen_tags_at_i[i])

    print "STAG types", "avg stags", "min stags", "max stags", "median stags" 
    print len(seen_tags), sum(tags_for_word) / float(len(tags_for_word)), min(tags_for_word), max(tags_for_word), sorted(tags_for_word)[len(tags_for_word)/2]
    
