package edu.columbia.nlp.gbdp;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.function.Consumer;
import java.util.stream.Stream;

/**
 * Created by daniel on 8/8/16.
 */
public class Tree2TreeProbs extends HashMap<String, Double> {

  public void populateFromFile(String inputFile) throws IOException {
    try (Stream<String> stream = Files.lines(Paths.get(inputFile))) {
      stream.forEach(new Consumer<String>() {

        @Override
        public void accept(String s) {
          String[] parts = s.trim().split(" ");
          String parent = parts[0];
          String child = parts[1];
          double prob = Double.parseDouble(parts[2]);
          put(String.format("%s_%s", parent, child), prob);
        }

      });
      System.err.println("Read tree-to-tree probabilities from file.");
    }
  }

  public Tree2TreeProbs(String inputFile) throws IOException {
    populateFromFile(inputFile);
  }

  public double get(String parent, String child) {
      Double result = get(String.format("%s_%s", parent, child));
      if (result == null) {
        result = get(String.format("%s_%s", parent, "UNK"));
        if (result == null) {
          return -20000;
        }
      }
      return result;
  }

}
