package edu.columbia.nlp.gbdp.forest;
import edu.columbia.nlp.gbdp.*;
import edu.columbia.nlp.gbdp.grammar.Grammar;
import edu.columbia.nlp.gbdp.grammar.NonterminalRuleToken;
import edu.columbia.nlp.gbdp.grammar.RuleToken;
import edu.columbia.nlp.gbdp.grammar.TerminalRuleToken;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.LinkedList;
import java.util.List;
import java.util.ArrayList;
import java.util.stream.Collectors;


/**
 * Created by daniel on 6/17/16.
 */
public class Node extends HasScore {
  Edge content;
  List<Hyperedge> branches;
  public int derived_string_anchor_index;

  public Node(Edge content, List<Hyperedge> branches) {
    this.content = content; // The completed parser Edge for this node
    setScore(content.getScore());
    this.branches = branches; // A list of the possible ways to decompose this node.
  }

  public Node(Edge content, Hyperedge split) {
    List<Hyperedge> l = new ArrayList<>();
    l.add(split);
    this.content = content;
    setScore(content.getScore());
    this.branches = l;
  }


  public String toString() {
    return content + " ==> " + branches;
  }

  //public String micaStyleDependencies() {

  //}

  public String printTree() {
    if (branches.isEmpty())
      return String.valueOf(content.getRule().rule_id);
    StringBuilder sb = new StringBuilder();
    sb.append("(");
    sb.append(content.getRule().rule_id);

    int position_index = 0;
    for (List<Node> positions : branches.get(0).getChildren()) {

      for (Node child : positions) {
        sb.append(" ");
        sb.append(content.getRule().rhs.get(position_index));
        sb.append(String.valueOf(position_index));
        sb.append(child.printTree());
      }
      position_index++;
    }
    sb.append(")");
    return sb.toString();
  }

  public String getTreeAndAnchorString() {
    StringBuilder sb = new StringBuilder();
    sb.append(this.content.getRule().rule_id);
    sb.append("/");
    for (RuleToken tok : content.getRule().rhs) {
      if (tok instanceof TerminalRuleToken) {
        sb.append(tok);
      }
    }
    return sb.toString().trim();
  }


  public List<String> getTerminalString() {
    List<String> result = new ArrayList<>();
    for (RuleToken tok : content.getRule().rhs) {
      if (tok instanceof TerminalRuleToken) {
        result.add(tok.value);
      }
    }
    return result;
  }

  public String getString() {
    StringBuilder sb = new StringBuilder();
    int i = 0;
    List<RuleToken> rhs = content.getRule().rhs;
    for (RuleToken tok : rhs) {
      if (tok instanceof TerminalRuleToken) {
        sb.append(tok.value);
        sb.append(" ");
      } else if (!branches.isEmpty())
        for (Node child : this.branches.get(0).getChildren().get(i)) {
          sb.append(child.getString());
        }
      i++;
    }
    return sb.toString();
  }


  private int recomputeStringIndices(int left) {
    int right = left;
    int i = 0;
    List<RuleToken> rhs = content.getRule().rhs;
    for (RuleToken tok : rhs) {
      if (tok instanceof TerminalRuleToken) {
        right++;
      } else if (!branches.isEmpty()) {
        for (Node child : this.branches.get(0).getChildren().get(i)) {
          right = child.recomputeStringIndices(right);
        }
      }
      i++;
    }

    Edge old_edge = this.content;
    this.content = new Edge(old_edge.getRule(), left, right, old_edge.getDot());
    return right;
  }

  public void recomputeStringIndices() {
    recomputeStringIndices(0);
  }


  public List<Node> getFlatChildren() {
    if (branches.isEmpty())
      return new ArrayList<>();
    return branches.get(0).getFlatChildren();
  }

  public ArrayList<Node> getSurfaceOrderNodeList() {
    ArrayList<Node> result = new ArrayList<>();
    getSurfaceOrderNodeListRec(result, 0);
    if (this.getTreeAndAnchorString().isEmpty()) { // hack for empty root node
      this.derived_string_anchor_index = -1;
    }
    return result;
  }

  private int getSurfaceOrderNodeListRec(ArrayList<Node> result, int min) {
    int min_index = min;
    int i = 0;
    List<RuleToken> rhs = content.getRule().rhs;
    boolean first_terminal = true;
    for (RuleToken tok : rhs) {
      if (tok instanceof TerminalRuleToken) {
        if (first_terminal) {
          derived_string_anchor_index = min_index;
          first_terminal = false;
          result.add(this);
        }
        min_index++;
      } else if (!branches.isEmpty())
        for (Node child : this.branches.get(0).getChildren().get(i)) {
          min_index = child.getSurfaceOrderNodeListRec(result, min_index);
        }
      i++;
    }
    return min_index;
  }

  public List<Edge> getPassiveEdges() {
    return getSurfaceOrderNodeList().stream().map(x -> x.content).collect(Collectors.toList());
  }


  public List<Traversal> getTraversals() {
    ArrayList<Traversal> result = new ArrayList<>();
    getTraversalsRec(result);
    return result;
  }

  public void getTraversalsRec(ArrayList<Traversal> result) {

    int right = content.getLeft();

    Edge active_edge = new Edge(content.getRule(), content.getLeft(), right, 0).forceShifts();
    // No traversal needed for predict
    Traversal predict_traversal = new Traversal(null, null, active_edge, 0.0);
    //System.out.println("adding traversal "+predict_traversal);
    result.add(predict_traversal);

    List<RuleToken> rhs = content.getRule().rhs;
    int i = 0;

    while (i < rhs.size()) {
      NonterminalRuleToken tok = (NonterminalRuleToken) rhs.get(i);
        if (branches.isEmpty() || branches.get(0).getChildren().get(i).isEmpty()) {
          // null adjunction
          Edge result_edge = new Edge(content.getRule(), content.getLeft(), right, i+1).forceShifts();
          right = result_edge.getRight();
          Traversal new_traversal = new Traversal(active_edge, null, result_edge, 0.0);
          //System.out.println("HERE5 " + new_traversal);
          result.add(new_traversal);

          // new active edge:
          active_edge = result_edge;
        } else { // regular adjunction or substitution
          int count = 0;
          for (Node child : this.branches.get(0).getChildren().get(i)) {
            //System.out.println(count++);
            right = child.content.getRight();

            if (tok.hasKleene) {
              Edge result_edge_no_noadj = new Edge(content.getRule(), content.getLeft(), right, i);
              Traversal new_traversal_no_noadj = new Traversal(active_edge, child.content, result_edge_no_noadj, 0.0);
              //System.out.println("HERE " + i + " "+ new_traversal_no_noadj);
              result.add(new_traversal_no_noadj);
              active_edge = result_edge_no_noadj;
            } else {
              Edge result_edge = new Edge(content.getRule(), content.getLeft(), right, i+1).forceShifts();
              Traversal new_traversal = new Traversal(active_edge, child.content, result_edge, 0.0);
              //System.out.println("HERE3 " + i + " "+ new_traversal);
              result.add(new_traversal);
              active_edge = result_edge;
            }

            child.getTraversalsRec(result);

          }

          if (tok.hasKleene) {
            Edge result_edge_with_noadj = new Edge(content.getRule(), content.getLeft(), right, i + 1).forceShifts();
            Traversal new_traversal_noadj = new Traversal(active_edge, null, result_edge_with_noadj, 0.0);
            //System.out.println("HERE2 " + i + " "+ new_traversal_noadj);
            result.add(new_traversal_noadj);
            active_edge = result_edge_with_noadj;
          }



        }
        i = active_edge.getDot();

    }
  }


  /**
   * Reconstruct binarized parser edges that could have produced this derivation.
   * Used for perceptron training.
   * @return
   */
  public List<Edge> getAllEdges() {
    ArrayList<Edge> result = new ArrayList<>();
    getAllEdgesRec(result);
    return result;
  }

  private void getAllEdgesRec(ArrayList<Edge> result) {


    result.add(content); // passive rule
    List<RuleToken> rhs = content.getRule().rhs;
    int right = content.getLeft();
    for (int i = 0; i < rhs.size(); i++) {
      RuleToken tok = rhs.get(i);
      if (tok instanceof NonterminalRuleToken) {
          if (branches.isEmpty() || branches.get(0).getChildren().get(i).isEmpty()) { // null adjunction
            Edge new_edge = new Edge(content.getRule(), content.getLeft(), right, i);
            result.add(new_edge);
          } else { // regular adjunction or substitution
            for (Node child : this.branches.get(0).getChildren().get(i)) { // All adjuncts in this position
              right = child.content.getRight();
              Edge new_edge = new Edge(content.getRule(), content.getLeft(), child.content.getLeft(), i); // all active rules
              result.add(new_edge);
              child.getAllEdgesRec(result);
            }
          }
      } else {
        right++;
      }
    }
  }
/*
  private int getDependencyNodesRec(List<DependencyNode> result, List<DependencyNode> local, int left) {

    int i = 0;
    int anchorpos = -1;
    int new_left = left;

    List<DependencyNode> mylocal = new ArrayList<>();

    for (RuleToken tok : content.getRule().rhs ) {
      if (tok instanceof TerminalRuleToken) {
        anchorpos = new_left;
        new_left += 1;
        DependencyNode anchor = new DependencyNode(tok.value, anchorpos);
        local.add(anchor);
        result.add(anchor);
      } else if (!branches.isEmpty()) {
        List<Node> children = this.branches.get(0).getChildren().get(i);
        if (children.size()==1 && anchorpos == -1 && false) {

          int tmp_left = new_left;
          new_left = children.get(0).getDependencyNodesRec(result, local, new_left);
          anchorpos = tmp_left;
        } else for (Node child : children) {
          new_left = child.getDependencyNodesRec(result, mylocal, new_left);
        }
      }
      i++;
    }
    for(DependencyNode node : mylocal) {
      node.setParentPosition(anchorpos);
    }
    return new_left;
}
*/




  private int getDependencyNodesRec(List<DependencyNode> result, List<DependencyNode> local_nodes, int left) {
    int min_index = left;
    int i =0;
    int first_terminal_index = -1;
    List<DependencyNode> new_local_nodes = new ArrayList<>();
    DependencyNode head = null;


    List<RuleToken> rhs = content.getRule().rhs;

    boolean hasTerminal = false;
    for (RuleToken tok: rhs) {
      if (tok instanceof TerminalRuleToken) {
        hasTerminal = true;
        break;
      }
    }

    for (RuleToken tok : rhs) {
      if (tok instanceof TerminalRuleToken) {
        derived_string_anchor_index = min_index;
        DependencyNode new_node =  new DependencyNode(tok.value, min_index);
        result.add(new_node);
        if (first_terminal_index == -1) { // parent node created by this rule
          first_terminal_index = min_index;
          local_nodes.add(new_node);
        } else { // all other terminals produce a child of this rule
          new_local_nodes.add(new_node);
        }
        min_index++;
      }
      else if (!branches.isEmpty()) {

        NonterminalRuleToken nt = (NonterminalRuleToken) tok;
        boolean foundTerminal = false;
        if ((!hasTerminal) && (!nt.hasKleene) && first_terminal_index==-1) {
          Node child = this.branches.get(0).getChildren().get(i).get(0); // child is a leaf
          if (child.getFlatChildren().size() == 0) {
            String value = child.content.getRule().rhs.get(0).value;
            first_terminal_index = min_index;
            DependencyNode new_node = new DependencyNode(value, min_index);
            local_nodes.add(new_node);
            result.add(new_node);
            foundTerminal = true;
            min_index++;
          }
        }

        if (!foundTerminal)
          for (Node child : this.branches.get(0).getChildren().get(i)) {
            min_index = child.getDependencyNodesRec(result, new_local_nodes, min_index);
          }
      }
      i++;
    }
    for (DependencyNode dchild : new_local_nodes) {
      dchild.setParentPosition(first_terminal_index);
    }
    return min_index;
  }

  public List<DependencyNode> getDependencyNodes() {
    List<DependencyNode> result = new ArrayList<DependencyNode>();
    List<DependencyNode> root_local = new ArrayList<DependencyNode>();
    getDependencyNodesRec(result,root_local, 0);
    for (DependencyNode root : root_local)
      root.setParentPosition(-1);
    return result;
  }


  /*
  public getDerivedString(List<String> result) {

    List<RuleToken> rhs = content.getRule().rhs;
    int i = 0;
    for (RuleToken tok : rhs) {
      if (tok instanceof TerminalRuleToken) {
        result.add(tok);
      } else if (!branches.isEmpty()){ // has more children
        for (Node child : branches.get(0).getChildren().get(i)) { // all subtrees at this position
          child.get
        }
      }
      i++;
    }

  }*/

  public List<String> getDerivedString() {

    return getSurfaceOrderNodeList().stream()
            .flatMap(node -> node.getTerminalString().stream())
            .collect(Collectors.toList());
  }


  public static List<Node> loadDerivations(Grammar grammar, InputStream derivation_input_stream) throws IOException{
    BufferedReader br = new BufferedReader(new InputStreamReader(derivation_input_stream));
    List<Node> result = new LinkedList<>();

    String line;
    while ((line=br.readLine())!=null) {

          InputStream stream = new ByteArrayInputStream(line.getBytes(StandardCharsets.UTF_8));
          DerivationParser parser = new DerivationParser(stream, grammar, null);
          try {
            Node derivation = parser.Derivation();
            derivation.recomputeStringIndices(); // make sure the start and end position of the edges in the derivation are set correctly.
            result.add(derivation);
          } catch (ParseException e) {
            result.add(null);
          }
        }

    return result;
  }

}
