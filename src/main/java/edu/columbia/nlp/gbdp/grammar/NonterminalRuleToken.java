package edu.columbia.nlp.gbdp.grammar;

/**
 * A single nonterminal token on a rule rhs.
 * Created by daniel on 6/13/16.
 */
public class NonterminalRuleToken extends RuleToken {
  public boolean hasKleene = false;
  public int index = -1;

  public NonterminalRuleToken(String value, boolean hasKleene) {
    super(value);
    this.hasKleene = hasKleene;
  }

  public NonterminalRuleToken(String value, boolean hasKleene, int theIndex) {
    this(value, hasKleene);
    index = theIndex;
  }

  @Override
  public String toString() {
    if (hasKleene) {
      if (index != -1) return String.format("%s$%d*", value, index);
        else return String.format("%s$*", value);
    }
    else{
      if (index != -1) return String.format("%s$%d", value, index);
        else return String.format("%s$", value);
    }
  }
}
