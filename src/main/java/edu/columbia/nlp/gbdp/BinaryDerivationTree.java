package edu.columbia.nlp.gbdp;

/**
 * Created by daniel on 8/2/16.
 */
public class BinaryDerivationTree<E> {

  E item;
  double score;
  BinaryDerivationTree<E> active;
  BinaryDerivationTree<E> passive;

  public BinaryDerivationTree(E item) {
    this.item = item;
  }

  public BinaryDerivationTree(E item, BinaryDerivationTree<E> active, BinaryDerivationTree<E> passive) {
    this(item);
    this.active = active;
    this.passive = passive;
  }

  public BinaryDerivationTree(E item, BinaryDerivationTree<E> active, BinaryDerivationTree<E> passive, double score) {
    this(item, active, passive);
    this.score = score;
  }

  public BinaryDerivationTree(E item, double score) {
    this(item);
    this.score = score;
  }

}
