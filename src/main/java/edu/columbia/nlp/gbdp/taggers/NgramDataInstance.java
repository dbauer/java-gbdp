package edu.columbia.nlp.gbdp.taggers;

import edu.columbia.nlp.util.averaged_perceptron.DataInstance;

import java.util.Arrays;

/**
 * Created by daniel on 7/21/16.
 */
  public class NgramDataInstance implements DataInstance {

    String label;
    Token[] tokens;
    public NgramDataInstance(String label, Token[] tokens) {
      this.label = label;
      this.tokens = tokens;
    }

    public String getDescription() {
      StringBuilder sb = new StringBuilder();
      for (int i=0; i<tokens.length; i++) {
        if (tokens[i]!=null) {
          sb.append(tokens[i].word);
          sb.append("/");
          sb.append(tokens[i].pos);
        } else {
          sb.append("#");
        }
          sb.append(" ");
      }
      return sb.toString().trim();
    }

    public String getLabel(){
      return label;
    }

    public Token[] getTokens(){
      return tokens;
    }

    public Token getToken(int i) {
      return tokens[i];
    }

    public boolean isCaps(int wordpos) {
      Token tok = tokens[wordpos];
      if (tok == null) return false;
      String first = tok.word.substring(0,1);
      return first.equals(first.toUpperCase());
    }

    // Feature extraction methods
    public String getWord(int wordpos) {
      Token tok = tokens[wordpos];
      if (tok == null) return "";
      String word = tok.word.toLowerCase();
      if (word.contains("-")) {
        String[] parts = word.split("-");
        if (parts.length==0) return word;
        return parts[parts.length-1];
      }
      return word;
    }

    public String getSuffix(int wordpos, int i) {
      String word = getWord(wordpos);
      if (word.length()<i)
        return word;
      return word.substring(word.length()-i, word.length());
    }

    public String getPrefix(int wordpos, int i) {
      String word = tokens[wordpos].word;
      if (word.length()<i)
        return word;
      return word.substring(0,i);
    }

    public String getPos(int wordpos) {
      Token tok = tokens[wordpos];
      if (tok == null) return "";
      return tokens[wordpos].pos;
    }

    public String getSimplePos(int wordpos) {
      Token tok = tokens[wordpos];
      if (tok == null) return "";
      return tokens[wordpos].pos.substring(0,1);
    }

    public boolean containsDash(int wordpos) {
      Token tok = tokens[wordpos];
      if (tok ==null)
        return false;
      return tok.word.contains("-");
    }

    public boolean isNumber(int wordpos) {
      Token tok = tokens[wordpos];
      if (tok == null) return false;
      String word = tok.word;
      if (word.matches("[0-9\\.,]+"))
        return true;
      if (word.matches("[0-9]+(s|th|nd|rd)"))
        return true;
      return false;
    }

    public String toString() {
        return Arrays.toString(tokens);
    }

  }
