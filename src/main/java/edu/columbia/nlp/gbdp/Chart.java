package edu.columbia.nlp.gbdp;

import edu.columbia.nlp.gbdp.Traversal;
import edu.columbia.nlp.gbdp.forest.Forest;
import edu.columbia.nlp.gbdp.forest.Node;
import edu.columbia.nlp.gbdp.forest.Hyperedge;
import edu.columbia.nlp.gbdp.grammar.Grammar;

import java.util.HashMap;
import java.util.Set;
import java.util.HashSet;
import java.util.Map;
import java.util.List;
import java.util.ArrayList;

/**
 * Created by daniel on 6/15/16.
 */
public class Chart {

  private final static boolean DEBUG = false;

  class ChartEntry extends HashMap<String, List<Edge>> {
  }

  private ChartEntry[][] active_chart;
  public ChartEntry[][] passive_chart;
  private int size;
  private Grammar grammar;

  public HashMap<Edge, List<Traversal>> backpointers;
  public HashMap<Edge, Traversal> last_backpointer; // This is the best current decomposition.
                                                    // Cache this to speed up feature extraction for structured prediction.

  public void printPassiveItems() {
    for (int i = 0; i < passive_chart.length; i++) {
      for (int j = 0; j < passive_chart[i].length; j++) {
        if (!passive_chart[i][j].isEmpty())
          System.out.println(i + " " + j + ":" + passive_chart[i][j]);
      }
    }
  }

  public Chart(int size, Grammar grammar) {
    this.grammar = grammar;
    this.size = size;
    active_chart = new ChartEntry[size][size];
    passive_chart = new ChartEntry[size][size];
    backpointers = new HashMap<>();
    last_backpointer = new HashMap<>();

    for (int i = 0; i < size; i++)
      for (int j = 0; j < size; j++) {
        active_chart[i][j] = new ChartEntry();
        passive_chart[i][j] = new ChartEntry();
      }
  }

  /**
   * Register a new way to build this edge.
   */
  public void addTraversal(Traversal t) {
    Edge result_edge = t.getResult();
    List<Traversal> bp_list = null;
    if (backpointers.containsKey(result_edge)) {
      bp_list = backpointers.get(result_edge);
    } else {
      bp_list = new ArrayList<>();
      backpointers.put(result_edge, bp_list);
    }
    bp_list.add(t);
    last_backpointer.put(result_edge, t);
  }

  /**
   * Insert an edge into the chart.
   *
   * @param e The edge to insert.
   */
  public void addEdge(Edge e) {

    ChartEntry cell = null;
    String nt = null;

    if (e.isActive()) { // active edge
      cell = active_chart[e.getLeft()][e.getRight()];
      nt = e.getOutside().value;
    } else {
      cell = passive_chart[e.getLeft()][e.getRight()];
      nt = e.getRule().lhs;
    }
    List<Edge> entry_list = null;

    if (!cell.containsKey(nt)) {
      entry_list = new ArrayList<Edge>();
      cell.put(nt, entry_list);
    } else {
      entry_list = cell.get(nt);
    }
    entry_list.add(e);

  }

  public ChartEntry getPassiveAt(int i, int j) {
    return passive_chart[i][j];
  }

  /**
   * Retrieve passive edges that start at index {@code i} and have label {@code lhs}.
   *
   * @param i   left position in the token string
   * @param lhs left hand side symbol to look for
   */
  public List<Edge> getPassiveWithStart(int i, String lhs) {
    List<Edge> result = new ArrayList<>();
    for (int j = 0; j < size; j++) {
      HashMap<String, List<Edge>> edges_at_ij = passive_chart[i][j];
      if (edges_at_ij.containsKey(lhs)) {
        result.addAll(edges_at_ij.get(lhs));
      }
    }
    return result;
  }

  /**
   * Retrieve passive edges that end at index {@code j} and outside symbol {@code outside}.
   *
   * @param j       right position in the token string
   * @param outside look up rules with this next nonterminal label
   */
  public List<Edge> getActiveWithEnd(int j, String outside) {
    List<Edge> result = new ArrayList<>();
    for (int i = 0; i < size; i++) {
      HashMap<String, List<Edge>> edges_at_ij = active_chart[i][j];
      if (edges_at_ij.containsKey(outside)) {
        result.addAll(edges_at_ij.get(outside));
      }
    }
    return result;
  }


  public Node get_one_best() {
    List<Edge> root_edges = getPassiveAt(0, passive_chart.length - 1).get(grammar.startsymbol);
    if (root_edges == null || root_edges.isEmpty()) return null; // No parse found

    double max_score = Double.NEGATIVE_INFINITY;
    Edge best_edge = null;
    for (Edge e : root_edges) {
      if (e.getScore() > max_score) {
        max_score = e.getScore();
        best_edge = e;
      }
    }

    Hyperedge split = new Hyperedge(best_edge.getRule());
    get_one_best_rec(best_edge, split);
    return new Node(best_edge, split);
  }


  private void get_one_best_rec(Edge e, Hyperedge split) {
    // TODO: Rewrite this to use the last_backpointer table instead.
    double max_score = Double.NEGATIVE_INFINITY;
    Traversal best_traversal = null;

    for (Traversal t : backpointers.get(e)) {
      if (t.getScore() > max_score) {
        max_score = t.getScore();
        best_traversal = t;
      }
    }

    /*
    if (Math.abs(best_traversal.getScore() - e.getScore()) > 1e-8) {
      System.out.println("mismatched score!");
      System.out.println(best_traversal.getScore());
      System.out.println(e.getScore());
      System.exit(1);
    }*/

    Edge passiveEdge = best_traversal.getPassive();
    Edge activeEdge = best_traversal.getActive();

    if (passiveEdge != null) {
        if (backpointers.containsKey(passiveEdge)) {
          Hyperedge subsplit = new Hyperedge(passiveEdge.getRule());
          get_one_best_rec(passiveEdge, subsplit);
          split.addChild(activeEdge.getDot(), new Node(passiveEdge, subsplit));
        } else {
          split.addChild(activeEdge.getDot(), new Node(passiveEdge, new Hyperedge(passiveEdge.getRule())));
        }
    }
    if (activeEdge != null && backpointers.containsKey(activeEdge)) {
      get_one_best_rec(activeEdge, split);
    }
 }

  public Node binaryDerivationTreeToForest(BinaryDerivationTree<Edge> tree) {
    if (tree == null)
      return null;
    List<Hyperedge> branches = new ArrayList<Hyperedge>();
    branches.add(binaryDerivationTreeHorizontalChildren(tree));
    return new Node(tree.item, branches);
  }

  private Hyperedge binaryDerivationTreeHorizontalChildren(BinaryDerivationTree<Edge> tree) {
    if (tree.passive == null) {
      if (tree.active == null) {
        return new Hyperedge(tree.item.getRule());
      } else {
        return binaryDerivationTreeHorizontalChildren(tree.active);
      }
    } else {
      Hyperedge children_so_far = binaryDerivationTreeHorizontalChildren(tree.active);
      Node subtree_for_passive = binaryDerivationTreeToForest(tree.passive);
      children_so_far.addChild(tree.item.getDot(), subtree_for_passive);
      return children_so_far;
    }
  }




  public Forest getForest() {

    Forest forest = new Forest();
    forest.addRoot(get_one_best());
    return forest;
    //    return new ForestBuilder().makeForest(grammar);
  }

  private class ForestBuilder {
    Forest forest;
    HashMap<Edge, List<Hyperedge>> seen_active_edges;

    public Forest makeForest(Grammar grammar) {
      forest = new Forest();
      seen_active_edges = new HashMap<>();
      HashMap<String, List<Edge>> root_entry = getPassiveAt(0, passive_chart.length - 1);
      //for (String nt : root_entry.keySet()) {
      String nt = grammar.startsymbol;
      List<Edge> root_edges = root_entry.get(nt);
      if (root_edges == null) // No parse found
        return null;
      for (Edge e : root_edges) {
        if (DEBUG) System.out.println("ROOT EDGE " + e);
        Node root = splitsForPassiveEdge(e);
        forest.addRoot(root);
      }
      return forest;
    }

    public Hyperedge make_hyperedge_copy(Hyperedge h) {
      Hyperedge result = new Hyperedge(h.rule);
      result.score = h.score;
      int i = 0;
      for (List<Node> nodes_for_position : h.getChildren()) {
        if (nodes_for_position != null) {
          for (Node n : nodes_for_position) {
            result.addChild(i, n);
          }
        }
      }
      return result;
    }

    public Node splitsForPassiveEdge(Edge e) {
      Node n = forest.getNodeForEdge(e);
      if (n == null) { // don't process nodes twice.
        if (DEBUG) {
          System.out.println("processing edge " + e);
        }
        Node new_node;
        List<Traversal> traversals = backpointers.get(e);
        if (traversals != null) {
          if (DEBUG) System.out.println(traversals.size() + " traversals.");
          List<Hyperedge> result = new ArrayList<>(100);
          for (Traversal t : traversals) { // all ways in which this edge could have been built
            List<Hyperedge> branches = horizontalChildren(t);
            result.addAll(branches);
          }
          new_node = new Node(e, result);
        } else {
          new_node = new Node(e, new ArrayList<Hyperedge>(100));
        }
        forest.addNode(new_node);
        return new_node;
      }
      return n;
    }


    List<Hyperedge> unary_split(Edge active) {
      if  (DEBUG) System.out.println("unary " + active);

      List<Hyperedge> result = new ArrayList<>();

      if (backpointers.containsKey(active)) {
        List<Traversal> traversals_for_active = backpointers.get(active);
        for (Traversal s : traversals_for_active) {
          List<Hyperedge> subedges = horizontalChildren(s);
          result.addAll(subedges);
        }
      }
      seen_active_edges.put(active, result);
      return result;
    }

    public List<Hyperedge> horizontalChildren(Traversal t) {
      if (DEBUG) System.out.println("calling horizontalChildren for " + t);
      Edge active = t.getActive();
      Edge passive = t.getPassive();

      if (passive == null) {

        List<Hyperedge> seen_result = seen_active_edges.get(active);
        if (seen_result != null) {
          if (DEBUG) System.out.println("SEEEN " + active);
          return seen_result;
        }
        return unary_split(active); // unary active item, just pass the list of hyperedge along.

      } else {

        List<Hyperedge> result = new ArrayList<Hyperedge>();
        // Compute subtrees
        Node passive_node = splitsForPassiveEdge(passive);

        List<Hyperedge> partial_splits = seen_active_edges.get(active);
        if (partial_splits == null) {
          partial_splits = unary_split(active);
        } else {
          if (DEBUG) System.out.println("Binary seen active "+active);
        }
        if (DEBUG)
        System.out.println("size of partial split list "+ partial_splits.size());


        if (!partial_splits.isEmpty()) {
          for (Hyperedge partial_split : partial_splits) {
            Hyperedge new_split = make_hyperedge_copy(partial_split);

            new_split.score += t.score;
            new_split.addChild(active.getDot(), passive_node);
            result.add(new_split);
          }
        } else { // No further active edges. Just append the list containing a single node for the passive item.
          Hyperedge hedge = new Hyperedge(active.getRule());
          hedge.score = t.score;
          hedge.addChild(active.getDot(), passive_node);
          result.add(hedge);
        }
        return result;

      }
    }
  }   // end of class ForestBuilder

}



