package edu.columbia.nlp.gbdp.parser;

import edu.columbia.nlp.gbdp.Edge;
import edu.columbia.nlp.gbdp.Traversal;
import edu.columbia.nlp.gbdp.grammar.Grammar;
import edu.columbia.nlp.gbdp.grammar.NonterminalRuleToken;
import edu.columbia.nlp.gbdp.grammar.RuleToken;
import edu.columbia.nlp.gbdp.grammar.TerminalRuleToken;
import edu.columbia.nlp.gbdp.taggers.Token;

import java.io.IOException;
import java.util.Properties;

/**
 * Created by bauer on 10/27/16.
 */
public class SupertagFactoredParser extends AbstractParser {


  public SupertagFactoredParser(Grammar grammar, Properties config) throws IOException {
    super(grammar, config);

  }

  @Override
  protected double scoreInitialEdge(Edge e) {
    return e.getScore();
  }

  @Override
  protected double scoreTraversal(Traversal t) {
    double res =  best_score.get(t.getActive()) + best_score.get(t.getPassive()) + t.getResult().getScore(); // this is a bit of a hack
    return res;
  }

  @Override
  protected double scoreNoAdjunction(Traversal t) {
    return best_score.get(t.getActive()) + t.getResult().getScore();
  }


  @Override
  double computeHeuristic(Edge e) {
    double result = 0.0;
    for (int i=0; i<e.getLeft(); i++)
      result += tokens.get(i)[0].getScore();
    for (int i=e.getRight(); i<tokens.size(); i++)
      result += tokens.get(i)[0].getScore();
    return result;
  }

  @Override
  Edge doShifts(Edge e) {

    if (!e.isActive())
      return e;

    if (e.getOutside() instanceof NonterminalRuleToken)
      return e;
    if (e.getRight() >= tokens.size())
      return null;

    int new_right = e.getRight();
    int new_dot = e.getDot();
    RuleToken tok = e.getRule().rhs.get(new_dot);
    double tag_score = 0.0;
    while (tok instanceof TerminalRuleToken && new_dot < e.getRule().rhs.size() && new_right < tokens.size()) {
      Boolean match = false;
      for (Token t : tokens.get(new_right)) {
        if (tok.value.equals(t.stag)) {
          match = true;
          tag_score = t.getScore();
          break;
        }
      }
      if (!match)
        return null;

      new_right++;
      new_dot++;
      if (new_dot < e.getRule().rhs.size())
        tok = e.getRule().rhs.get(new_dot);
    }

    if (new_right > tokens.size())
      return null;

    Edge result = new Edge(e.getRule(), e.getLeft(), new_right, new_dot, tag_score); // this is part 2 of the hack
    result.setHeuristic(computeHeuristic(e));
    return result;
  }

}
