#!/usr/bin/python
import sys
from deps_tools import DepsFile
from collections import defaultdict 

if __name__=="__main__":

    stags_to_pos = defaultdict(set) 

    with open(sys.argv[1],'r') as deps_file: 
        deps = DepsFile(deps_file)

        for gold_sentence in deps.sentence_iterator():

            gold_tuples = [(gold_sentence[0][x].tree, gold_sentence[0][x].pos) for x in gold_sentence[0]]
            for k,v in gold_tuples:
                stags_to_pos[k].add(v)
        
        for k,v in stags_to_pos.items():
            print k, " ".join(v)
            

