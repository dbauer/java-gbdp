package edu.columbia.nlp.gbdp;

import edu.columbia.nlp.gbdp.grammar.NonterminalRuleToken;
import edu.columbia.nlp.gbdp.grammar.Rule;
import edu.columbia.nlp.gbdp.grammar.RuleToken;
import edu.columbia.nlp.gbdp.grammar.TerminalRuleToken;
import edu.columbia.nlp.gbdp.taggers.Token;

import java.util.List;

/**
 * An active or passive edge on the agenda.
 * The "score" of the Edge is the maximum probability of parsing this edge.
 * (one could imagine a heuristic parser in which the score also includes an estimate of
 *  a parse for the entire sentence that includes the score)
 * Created by daniel on 6/13/16.
 */
public class Edge extends HasScore{
  private final int left;
  private final int right;
  private final int dot;
  private final Rule rule;
  private int hash_cache;
  private int anchorPos; // if the anchor was seen, this is its token position in the input.
  private String head;


  public Edge(Rule rule, int left, int right, int dot) {
    this.rule = rule;
    this.left = left;
    this.right = right;
    this.dot = dot;
    hash_cache = 37 * rule.hashCode() + 53 * left + 59 * right + 79 * dot;
  }

  public Edge(Rule rule, int left, int right, int dot, double score) {
    this(rule, left, right, dot);
    this.score = score;
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("<");
    sb.append(rule.lhs);

    sb.append(" -> ");
    int i = 0;
    for (RuleToken t : rule.rhs) {
      sb.append(" ");
      if (i == dot) sb.append(". ");
      sb.append(t);
      i++;
    }
    if (i == dot) sb.append(" . ");
    sb.append(String.format(" [%d,%d]", left, right));
    sb.append(String.format(" %e >", score));
    return sb.toString();
  }

  public int getLeft() {
    return left;
  }

  public int getRight() { return right; }

  public int getDot() {
    return dot;
  }

  public Rule getRule() {
    return rule;
  }

  @Override
  public double getScore() {
    return score;
  }

  public void setScore(double score) {
    this.score = score;
  }

  /**
   * Returns True if the edge is active, otherwise False
   * if this is a passive edge.
   */
  public boolean isActive() {
    return dot < rule.size();
  }

  public RuleToken getOutside() {
    return rule.rhs.get(dot);
  }

  /**
   * Perform all shift operations permitted under the input {@code tokens}.
   * Return a new Edge with all shifts applied.
   all
   * @param tokens
   * @return
   */
  public Edge allShifts(List<Token[]> tokens){


    if (isActive()) {
      if (getOutside() instanceof NonterminalRuleToken) {

        if (right > tokens.size()) // this should never happen!
            return null;
        else
            return this;
      }
      if (right >= tokens.size())
        return null;


      int first_terminal_pos = -1;
      int new_right = right;
      int new_dot = dot;
      RuleToken tok = rule.rhs.get(new_dot);
      double tag_score  = 0.0;
      while (tok instanceof TerminalRuleToken && new_dot < rule.rhs.size() && new_right <= tokens.size()) {

        if (first_terminal_pos==-1)
          first_terminal_pos = new_right;

        Boolean match = false;
        for (Token t : tokens.get(new_right)) {
          if (tok.value.equals(t.stag)) {
            match = true;
            tag_score += t.getScore();
            break;
          }
        }
        if (!match)
          return null;

        new_right++;
        new_dot++;
        if (new_dot < rule.rhs.size())
          tok = rule.rhs.get(new_dot);

      }

      //if (new_dot == rule.rhs.size())
      //  return new Edge(rule, left, new_right, new_dot, score);
      if (new_right > tokens.size())
        return null;
      double new_score = score + tag_score;
      Edge result =new Edge(rule, left, new_right, new_dot, new_score);
      result.setAnchorPos(first_terminal_pos);
      return result;
    } else {
      return this;
    }

  }

  public int getAnchorPosition() {
    return anchorPos;
  }

  public void setAnchorPos(int anchor) {
    anchorPos = anchor;
  }

  public boolean seenAnchor() {
    return dot > rule.anchorPos;
  }

  public String getAnchor() {return this.rule.rhs.get(this.rule.anchorPos).value;};

  public Edge forceShifts() {

    if (isActive()) {
      if (getOutside() instanceof NonterminalRuleToken)
        return this;

      int new_right = right;
      int new_dot = dot;

      while (rule.rhs.get(new_dot) instanceof TerminalRuleToken) {
        new_dot++;
        new_right++;
      }
      return new Edge(rule, left, new_right, new_dot, score);
    } else {
      return this;
    }


  }

  /**
   * Complete this active edge with a passive edge, returning a new edge
   * @param p
   * @return
   */
  public Edge complete(Edge p) {
    if (((NonterminalRuleToken) getOutside()).hasKleene) {
      return new Edge(rule, left, p.right, dot);
    } else {
      return new Edge(rule, left, p.right, dot + 1);
    }
  }

  public Edge skip() {
    return new Edge(rule, left, right, dot+1);
  }

  //public void setScore(double score) {
  //  this.score = score;
  //}

  public int hashCode(){
    return hash_cache;
  }

  public boolean equals(Object other) {
    if (!(other instanceof Edge))
      return false;
    Edge otherEdge = (Edge) other;
    return otherEdge.left == left && otherEdge.right == right &&
            otherEdge.rule.rule_id == rule.rule_id && otherEdge.dot == dot;
  }

}

