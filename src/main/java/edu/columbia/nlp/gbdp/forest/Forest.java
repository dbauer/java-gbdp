package edu.columbia.nlp.gbdp.forest;

import edu.columbia.nlp.gbdp.Edge;
import edu.columbia.nlp.gbdp.ForestScorer;
import edu.columbia.nlp.gbdp.NBestBeamPQ;
import edu.columbia.nlp.gbdp.RatioBeamPQ;
import edu.columbia.nlp.gbdp.grammar.NonterminalRuleToken;
import edu.columbia.nlp.gbdp.grammar.RuleToken;
import edu.columbia.nlp.util.CartesianProductGenerator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by daniel on 6/17/16.
 */
public class Forest {

  public final static boolean DEBUG =  false;

  private List<Node> roots;

  private Map<Edge, Node> edge_to_node; // Edge is a parser edge

  public Forest() {
    roots = new ArrayList<>();
    edge_to_node = new HashMap<>();
  }

  public List<Node> getRoots() {
    return roots;
  }

  public void addNode(Node n) {
    edge_to_node.put(n.content, n);
  }

  public boolean hasEdge(Edge edge) {
    return edge_to_node.containsKey(edge);
  }

  public Node getNodeForEdge(Edge edge) {
    if (hasEdge(edge))
      return edge_to_node.get(edge);
    else
      return null;
  }

  public void addRoot(Node n) {
    roots.add(n);
  }


  public Map<Node, ArrayList<Node>> nbest_cache;

  public List<Node> nBest(int n, ForestScorer scorer) {

    nbest_cache = new HashMap<>();

    NBestBeamPQ<Node> pq = new NBestBeamPQ<>(n);
    for (Node root : roots)
      for (Node tree : nBestRec(n, root,0, scorer))
        pq.add(tree);
    List<Node> result = new ArrayList<>();
    while (!pq.empty())
      result.add(pq.pollMax());
    return result;
  }

  private ArrayList<Node> nBestRec(int n, Node node, int indent, ForestScorer scorer) {

    if (nbest_cache.containsKey(node)) {
      if (DEBUG) System.out.println("seen node before.");
      return nbest_cache.get(node);
    }

    //if (DEBUG) System.out.println("Computing "+n + "-best for "+node);

    //for (int i=0; i<indent; i++) System.out.print(" ");
    // Leaf case
    if (node.branches.isEmpty()) {
      //for (int i=0; i<indent; i++) System.out.print(" ");
      ArrayList<Node> result = new ArrayList<>();
      result.add(node);
      nbest_cache.put(node, result);
      return result;
    }

    NBestBeamPQ<Node> pq = new NBestBeamPQ<>(n);
    for (Hyperedge branch : node.branches) {

      //UGH :\ There must be a better way

      ArrayList<Integer> child_idx_to_position_idx = new ArrayList<>();

      List<Node> children = new ArrayList<>();
      int position_idx = 0;
      for (List<Node> position : branch.getChildren()) {
        for (Node child : position) {
          child_idx_to_position_idx.add(position_idx);
          children.add(child);
        }
          position_idx++;
      }

      ArrayList<ArrayList<Node>> nBestEachChild = new ArrayList<>();
      if (DEBUG) System.out.println(children.size());
      for (Node child : children) {
        if (DEBUG) System.out.println("Node " + child.branches.size());
        nBestEachChild.add(nBestRec(n, child, indent + 2, scorer));
        if (DEBUG) System.out.println("done with " + child);
      }

      if (DEBUG)
        for (ArrayList<Node> l: nBestEachChild)
          System.out.print(l.size()+" ");

      // get nbest possible ways to build each child
      for (List<Node> possibility : new CartesianProductGenerator<Node>(nBestEachChild)) {
        Hyperedge hedge = new Hyperedge(branch.rule);

        int child_idx = 0;
        for (Node child: possibility) {
         hedge.addChild(child_idx_to_position_idx.get(child_idx++), child);
        }

        hedge.score = scorer.score(hedge);
        ArrayList<Hyperedge> dummylist = new ArrayList<>(); // only one hyperedge child in a tree
        dummylist.add(hedge);

        Node new_node = new Node(node.content, dummylist);
        new_node.setScore(hedge.score);
        pq.add(new_node);
      }
    }

    ArrayList<Node> result = pq.asArrayList();
    nbest_cache.put(node, result);
    return result;
  }

  public void printForest() {

    for (Node n : edge_to_node.values()) {
      if (n.branches.size() > 0) {

        StringBuilder sb = new StringBuilder();
        sb.append(n.content);
        sb.append(" -> ");
        for (Hyperedge split : n.branches) {
          sb.append("[");
          for (Node m : split.getFlatChildren()) {
            sb.append(" ");
            if (m == null)
              sb.append(m);
            else
              sb.append(m.content);
          }
          sb.append("] ");
          sb.append(Math.exp(split.score));
        }
        System.out.println(sb.toString());
      }
    }
  }

}
