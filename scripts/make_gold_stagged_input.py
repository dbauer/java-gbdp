#!/usr/bin/python
import sys
from deps_tools import DepsFile

"""
Read a Mica .deps file (specified as command line argument) and write out sequences of super tags for each sentence. 
(one sentence per line)
"""
if __name__=="__main__":
    with open(sys.argv[1],'r') as f:
        corpus = DepsFile(f)
        for sentence in corpus.sentence_iterator():
            print " ".join([sentence[0][x].tree for x in sentence[0]])
