package edu.columbia.nlp.util;
import org.junit.Test;


import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

/**
 * Created by daniel on 6/30/16.
 */

public class MinMaxHeapTest {

  @Test
  public void testCreateHeap(){
    DoubleEndedPriorityQueue<Integer> depq = new MinMaxHeap<>(100);
  }

  private Integer findMin(List<Integer> list) {
    Integer min = Integer.MAX_VALUE;
    for (Integer i : list) {
      if (i < min)
        min = i;
    }
    return min;
  }

  private Integer findMax(List<Integer> list) {
    Integer max = Integer.MIN_VALUE;
    for (Integer i : list) {
      if (i > max)
        max = i;
    }
    return max;
  }

  @Test
  public void testInsertDelete(){
    Random r = new Random();
    List<Integer> testList = new ArrayList<>();
    MinMaxHeap<Integer> depq = new MinMaxHeap<>();
    for (int i=0; i<10; i++) {
      Integer n = r.nextInt();
      testList.add(n);
      depq.add(n);
    }
    for (int i=0; i<10; i++) {
      if (r.nextBoolean()) {
        Integer min = findMin(testList);
        assertEquals(min,depq.pollMin());
        testList.remove(min);
      } else {
        Integer max = findMax(testList);
        assertEquals(max,depq.pollMax());
        testList.remove(max);
      }
    }
  }

  @Test
  public void testAlternateInsertDelete(){
    for (int k=0; k<10;k++) {
      Random r = new Random();
      List<Integer> testList = new ArrayList<>();
      MinMaxHeap<Integer> depq = new MinMaxHeap<>();
      for (int i = 0; i < 10; i++) {
        Integer n = r.nextInt(10);
        testList.add(n);
        depq.add(n);
      }

      for (int i = 0; i < 5; i++) {
        if (r.nextBoolean()) {
          Integer min = findMin(testList);
          assertEquals(min, depq.pollMin());
          testList.remove(min);
        } else {
          Integer max = findMax(testList);
          assertEquals(max, depq.pollMax());
          testList.remove(max);
        }
      }

      for (int i = 0; i < 5; i++) {
        Integer n = r.nextInt(10);
        testList.add(n);
        depq.add(n);
      }
      for (int i = 0; i < 5; i++) {
        if (r.nextBoolean()) {
          Integer min = findMin(testList);
          assertEquals(min, depq.pollMin());
          testList.remove(min);
        } else {
          Integer max = findMax(testList);
          assertEquals(max, depq.pollMax());
          testList.remove(max);
        }
      }
    }
  }

  @Test
  public void testResize() {
    MinMaxHeap<Integer> depq = new MinMaxHeap<>();
    for(int i=0; i<11;i++)
      depq.add(new Integer(i));
    for(int i=0; i<11;i++)
      assertEquals(new Integer(i), depq.pollMin());
    assertNull(depq.pollMin());
  }

}
