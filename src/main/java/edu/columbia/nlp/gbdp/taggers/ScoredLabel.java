package edu.columbia.nlp.gbdp.taggers;

import edu.columbia.nlp.gbdp.HasScore;

/**
 * Created by daniel on 7/27/16.
 */
public class ScoredLabel<T> extends HasScore {

  private T label;

  public ScoredLabel(T label, double score) {
    setScore(score);
    this.label = label;
  }

  public T getLabel(){
    return label;
  }

}
