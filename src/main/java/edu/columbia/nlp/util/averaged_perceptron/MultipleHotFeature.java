package edu.columbia.nlp.util.averaged_perceptron;

import java.io.Serializable;
import java.util.HashMap;
import java.util.function.Function;
import java.util.List;

/**
 * Created by daniel on 10/15/16.
 */
public class MultipleHotFeature<S,T> implements Serializable {

  private static final long serialVersionUID = 5421581851223979305L;

  private transient Function<S, List<T>> extractor;


  /**
   * @serial
   */
  private HashMap<T, Integer> concrete_feature_map;
  private int capacity;

  public MultipleHotFeature(Function<S,List<T>> extractor, int capacity) {
    this.extractor = extractor;
    this.capacity = capacity;
    concrete_feature_map = new HashMap<T, Integer>(SparseVector.DEFAULT_CAPACITY);
  }

  public MultipleHotFeature(Function<S,List<T>> extractor, HashMap<T, Integer> concrete_feature_map, int capacity) {
    this(extractor, capacity);
    setConcreteFeatureMap(concrete_feature_map);
  }

  public void setConcreteFeatureMap(HashMap<T, Integer> concrete_feature_map) {
    this.concrete_feature_map = concrete_feature_map;
    if (concrete_feature_map.size() > capacity)
      throw new FeatureDimensionsExceededError();
  }

  public MultipleHotFeature<S,T> spawn(Function<S,List<T>> extractor) {
    return new MultipleHotFeature<S,T>(extractor, concrete_feature_map, capacity);
  }

  public int getCapacity() {
    return capacity;
  }

  public int getLength(){
    return concrete_feature_map.size();
  }

  public SparseVector apply(S input) {
    return applyWithOffset(input, 0);
  }

  public SparseVector applyWithOffset(S input, int offset) {
    SparseVector result = new SparseVector();
    List<T> concrete_features = extractor.apply(input);

    if (concrete_features == null)
      return result;

    for (T concrete_feature : concrete_features) {
      Integer index = concrete_feature_map.get(concrete_feature);
      if (index == null) {
        int new_index = concrete_feature_map.size();
        if (new_index >= capacity)
          throw new FeatureDimensionsExceededError(concrete_feature.toString() + " " + String.valueOf(capacity));
        concrete_feature_map.put(concrete_feature, new_index);
        result.put(new_index + offset, 1.0);
      } else {
        result.put(index + offset, 1.0);
      }
    }
    return result;
  }

  public Function<S,List<T>> getExtractor(){
    return extractor;
  }

  public void setExtractor(Function<S,List<T>> extractor){
    this.extractor = extractor;
  }


}
