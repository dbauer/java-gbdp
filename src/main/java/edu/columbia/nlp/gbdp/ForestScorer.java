package edu.columbia.nlp.gbdp;

import edu.columbia.nlp.gbdp.forest.Hyperedge;
import edu.columbia.nlp.gbdp.forest.Node;
import edu.columbia.nlp.gbdp.grammar.Grammar;
import edu.columbia.nlp.gbdp.grammar.NonterminalRuleToken;
import edu.columbia.nlp.gbdp.grammar.RuleToken;
import edu.columbia.nlp.gbdp.grammar.TerminalRuleToken;

import java.util.List;

/**
 * Created by bauer on 7/8/16.
 */
public class ForestScorer {

    Grammar grammar;

    public ForestScorer(Grammar grammar) {
        this.grammar = grammar;
    }

    public double score(Hyperedge hyperedge) {
        double score = 0.0;
        List<List<Node>> children = hyperedge.getChildren();

        int position_idx = 0;
        for (List<Node> position : children) {
            for (Node child : position) {
                score += child.getScore();
            }
            RuleToken tok = hyperedge.rule.rhs.get(position_idx);
            if (tok instanceof NonterminalRuleToken) {
                NonterminalRuleToken nt = (NonterminalRuleToken) tok;
                if (nt.hasKleene)
                    score += grammar.getNoAdjScore(nt.value);
            }
            position_idx++;
        }

        score += hyperedge.rule.weight;
        return score;
    }

}
