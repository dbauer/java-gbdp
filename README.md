# Grammar Based Dependency Parser #

An implementation of a chart parser for generative dependency grammar [http://www.anthology.aclweb.org/W/W04/W04-1503.pdf]((Rambow & Nasr, 2004)).

### Downloading the software ###
Via git: 

```$ git clone git@bitbucket.org:dbauer/java-gbdp.git```

### Building the software ###

Using maven 3. 

```$ mvn clean compile```

### Running the parser ###

```$ java -cp $GBDP_ROOT/target/classes/ edu.columbia.nlp.gbdp.Main -gr [grammar file] [input file]```