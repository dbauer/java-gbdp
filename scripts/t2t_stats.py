#!/usr/bin/python
import sys
from deps_tools import DepsFile
from collections import defaultdict
import numpy as np

from goodturing import simpleGoodTuringProbs 

"""
Read a Mica .deps file (specified as command line argument) and compute smoothed probabilities P(t|s), representing the probability for 
s to substitue or adjoin into a tree t (regardless of the adjunction or substitution site). 
"""

def sgtZ(sortedCounts, countsOfCounts):

    # TODO: check non-zeroes

    # For each count j, set Z[j] to the linear interpolation of i,j,k, where 
    # is the greatest observed count less than i and k is the smallest observed
    # count greater than j.
    Z = {}
    for (jIdx, j) in enumerate(sortedCounts):
        if jIdx == 0:
            i = 0
        else:
            i = sortedCounts[jIdx-1]
        if jIdx == len(sortedCounts)-1:
            k = 2*j - i # why? 
        else:
            k = sortedCounts[jIdx+1]
        Z[j] = countsOfCounts[j] / (0.5 * (k-i))
    return Z

def ll_regression(a, b):
    alog = np.log(a)
    at = np.array([np.log(a), [1]*len(a)]).transpose()
    bt = np.array(np.log(b)).transpose()
    x, residuals, rank, s = np.linalg.lstsq(at, bt)
    return x

if __name__=="__main__":

    counts = defaultdict(lambda: defaultdict(int))

    with open(sys.argv[1],'r') as f:
        corpus = DepsFile(f)
        for sentences in corpus.sentence_iterator():
            sentence = sentences[0]
            for deps_id in sentence: 
                tree = sentence[deps_id]
                counts[tree.parent_tree][tree.tree] += 1
       
 
    for parent_tree in counts: 

        probs, p0 = simpleGoodTuringProbs(counts[parent_tree])

        print parent_tree, "UNK", p0
        for k,v in probs.iteritems(): 
            print parent_tree, k, v

        continue 

        count_of_counts = defaultdict(int)
        sum_for_parent = 0

        for child_tree in counts[parent_tree]:
            count = counts[parent_tree][child_tree]
            count_of_counts[count] += 1
            sum_for_parent += count 
        if (not 1 in count_of_counts):
            count_of_counts[1] = 1
       
        print dict(count_of_counts)
        sorted_counts = sorted(count_of_counts.keys())
        z_dict =sgtZ(sorted_counts, count_of_counts)
        print z_dict

        x1, x2 = ll_regression(z_dict.keys(), z_dict.values())
        print(x1, x2)
 
        smoothed_counts = {}
        use_y = False
        for freq in sorted_counts:
            y = freq * np.exp(x1*np.log(freq+1) + x2) / np.exp(x1*np.log(freq) + x2)

            if (freq+1) not in count_of_counts or count_of_counts[freq+1]== 0:
                useY = True

            if useY:
                smoothed_counts[freq] = y
            else: 
                p_empirical = (float(freq+1) * count_of_countss[freq+1]) / count_of_countss[freq]

                t = confidenceLevel * np.sqrt(float(freq+1)**2 * (count_of_countss[freq+1] / count_of_countss[freq]**2)  * (1.0 + (count_of_countss[freq+1] /count_of_countss[freq])))
                if abs(p_empirical-y) > t: 
                    smoothed_counts = p_empirical
                else:
                    smoothed_counts = y
                    useY = True
        
        unseen_prob = smoothed_counts[1] / float(sum_for_parent) #p0

        final_probs = {}
        smoothed_total = 0.0
        print smoothed_counts
        for freq, smoothed_freq in smoothed_counts.iteritems():
            smoothed_total += count_of_counts[freq] * smoothed_freq
        print parent_tree, "UNK", unseen_prob
        for child_tree, child_freq in counts[parent_tree].iteritems():
            final_probs[child_tree] = (1.0 - unseen_prob) * (smoothed_counts[child_freq]/ smoothed_total)    
            print parent_tree, child_tree, counts[parent_tree][child_tree], final_probs[child_tree]
    
