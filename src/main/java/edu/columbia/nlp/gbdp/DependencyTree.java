package edu.columbia.nlp.gbdp;

import edu.columbia.nlp.gbdp.forest.Node;

import java.util.Arrays;
import java.util.List;

/**
 * Created by daniel on 7/12/16.
 */
public class DependencyTree {

  private DependencyNode[] position_to_node;

  public DependencyTree(Node root) {
    List<DependencyNode> nodes = root.getDependencyNodes();
    position_to_node = new DependencyNode[nodes.size()];
    for( DependencyNode dn :nodes)
       position_to_node[dn.getPosition()]=dn;
  }

  public String micaRepresentation() {
    StringBuilder sb = new StringBuilder();
    for (int i=0; i<position_to_node.length; i++) {
      DependencyNode node = position_to_node[i];
      sb.append(node.getPosition()+1);
      sb.append(" ");
      if (node.getParentPosition()>=0)
        sb.append(node.getParentPosition()+1);
      else
        sb.append(node.getPosition()+1);
      sb.append(" ");
      sb.append(node.getTree());
      sb.append(" ");
      if (node.getParentPosition()>=0)
        sb.append(position_to_node[node.getParentPosition()].getTree());
      else
        sb.append(node.getTree());
      sb.append("\n");
    }
    return sb.toString();
  }

}
