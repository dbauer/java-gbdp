package edu.columbia.nlp.util;

/**
 * Created by bauer on 10/28/16.
 */
public class Log {

  static final double LOGTOLERANCE_F = 20.0;

  /**
   * Returns the log of the sum of two numbers, which are
   * themselves input in log form.  This uses natural logarithms.
   * Reasonable care is taken to do this as efficiently as possible
   * (under the assumption that the numbers might differ greatly in
   * magnitude), with high accuracy, and without numerical overflow.
   * Also, handle correctly the case of arguments being -Inf (e.g.,
   * probability 0).
   *
   * @param lx First number, in log form
   * @param ly Second number, in log form
   * @return log(exp(lx) + exp(ly))
   */
  public static double logAdd(double lx, double ly) {
    double max, negDiff;
    if (lx > ly) {
      max = lx;
      negDiff = ly - lx;
    } else {
      max = ly;
      negDiff = lx - ly;
    }
    if (max == Double.NEGATIVE_INFINITY) {
      return max;
    } else if (negDiff < -LOGTOLERANCE_F) {
      return max;
    } else {
      return max + (double) Math.log(1.0 + Math.exp(negDiff));
    }
  }

}
