package edu.columbia.nlp.gbdp.taggers;

import edu.columbia.nlp.util.averaged_perceptron.AveragedPerceptron;
import edu.columbia.nlp.util.averaged_perceptron.AveragedPerceptronModel;
import edu.columbia.nlp.util.averaged_perceptron.FeatureSet;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Stream;

/**
 * Created by daniel on 7/18/16.
 */
public class PosTagger {

  AveragedPerceptron<NgramDataInstance> classifier;

  private static String[] split(String s){
    return s.split("\\s");
  }

  private static String concatArray(String[] strings) {
    StringBuilder sb = new StringBuilder();
    for (int i = 0; i<strings.length; i++) {
      sb.append(strings[i]);
      sb.append(" ");
    }
    return sb.toString().trim();
  }

  private AveragedPerceptronModel<NgramDataInstance> read_model_from_file(File model_file) throws IOException{
    AveragedPerceptronModel<NgramDataInstance> model;
    try {
      FileInputStream is = new FileInputStream(model_file);
      ObjectInputStream ois = new ObjectInputStream(is);
      model = (AveragedPerceptronModel<NgramDataInstance>) ois.readObject();
    } catch (ClassNotFoundException e) {
      e.printStackTrace();
      throw new IOException();
    }
    return model;
  }

  public PosTagger(File model_file) throws IOException {

    // Template that contains feature extraction lambdas
    FeatureSet<NgramDataInstance> template = PosFeatureExtractor.makeFeatureSet();

    AveragedPerceptronModel loaded_model = read_model_from_file(model_file);
    FeatureSet<NgramDataInstance> feature_definition = loaded_model.getFeatures();
    feature_definition.setExtractorsFromTemplate(template);
    classifier = new AveragedPerceptron(feature_definition, loaded_model.getWeights());
  }


  public String[] tag(String[] words) {

    String[] tags = new String[words.length];
    Token[] context = new Token[4];
    context[3] = new Token(words[0]);

    for (int i=0; i<words.length;i++) {
      context[0] = context[1];
      context[1] = context[2];
      context[2] = context[3];
      if (i<words.length-1)
        context[3] = new Token(words[i+1]);
      else
        context[3] = null;
      NgramDataInstance inst = new NgramDataInstance(null, context);
      String predicted = classifier.predict(inst);
      tags[i] = predicted;
      context[2].pos = predicted;
    }
    return tags;
  };

  public List<Token> tag(List<Token> tokens) {
    return Arrays.asList(tag(tokens.toArray(new Token[tokens.size()])));
  }

  public Token[] tag(Token[] tokens) {
    String[] words = new String[tokens.length];
    for (int i=0; i<tokens.length; i++)
      words[i] = tokens[i].word;

    String[] tags = tag(words);

    Token[] result = new Token[tokens.length];
    for (int i=0; i<tokens.length; i++)
      result[i] = new Token(tokens[i].word, tags[i], tokens[i].stag);

    return result;
  }

  public static void main(String[] argv) throws IOException {
    File model_file = new File(argv[0]);
    PosTagger tagger = new PosTagger(model_file);


    System.err.println("POS tagger ready...");

    String inputFile = argv[1];
    try (Stream<String> stream = Files.lines(Paths.get(inputFile))) {
      stream.forEach(new Consumer<String>() {
        @Override
        public void accept(String s) {
          String[] result = tagger.tag(split(s));
          System.out.println(concatArray(result));
        }
      });
    } catch (IOException e) {
      System.out.println(String.format("Error reading from input file %s", inputFile));
      e.printStackTrace();
      System.exit(1);
    }

  }

}
