#!/usr/bin/python
import sys
from deps_tools import DepsFile

class DependencyEdge():
    def __init__(self, pos, parent_pos, tree, parent_tree):
        self.pos = int(pos)
        self.parent_pos = int(parent_pos)
        self.tree =  tree
        self.parent_tree = parent_tree
        

def sentence_iterator(f):
    
    line = f.readline()

    sentence = []
    no_parse = False
    time_out = False
    first = True
    while line:
        if line.startswith("#"):
            if line.strip() == "# No parses found.":
                no_parse = True
            elif line.strip() == "# Timeout.":
                time_out = True
        elif line.startswith("...END..."):
            pass
        elif line.startswith("...EOS..."):
            if first: 
                if no_parse: 
                    yield "NP"
                elif time_out:
                    yield "TO"
                else:  
                    yield sentence
                sentence = []
                no_parse = False
                time_out = False
                first = False
            else: 
                first = True
        else: 
            sentence.append(DependencyEdge(*line.strip().split()))
        line = f.readline()


def evaluate_heads(gold_heads, test_heads):

    correct = 0
    if not test_heads:
        return 0, len(gold_heads), 0.0

    for i in range(len(gold_heads)):
        if gold_heads[i] == test_heads[i]:
            correct += 1
    return correct, len(gold_heads), float(correct)/len(gold_heads)

"""
Read a Mica .deps file (specified as command line argument) and write out sequences of super tags for each sentence. 
(one sentence per line)
"""
if __name__=="__main__":

    commas = [ 't1337', 't2530', 't3502', 't20', 't4697', 't736', 't1416', 't173', 't408', 't3428', 't84', 't76', 't360', 't14', 't968', 't800', 't40', 't55', 't110', 't3129', 't3264', 't148', 't4045', 't21', 't1003', 't63', 't397', 't2530', 't183', 't4402', 't1261', 't1029', 't2709', 't132', 't2125', 't2055', 't251', 't776', 't14', 't2437', 't527', 't3526', 't138' ]
   
    with open(sys.argv[1],'r') as gold, open(sys.argv[2],'r') as test :
        gold_corpus = DepsFile(gold)
        test_corpus = sentence_iterator(test)


        total_count = 0
        length_sum = 0
    
        error_length_sum = 0

        correct_sum = 0
        accuracy_sum = 0.0

        total_no_parse = 0
        total_timeout = 0
    
        for gold_sentence in gold_corpus.sentence_iterator():
            test_sentence = test_corpus.next()            

            comma_positions = [i for i in gold_sentence[0] if gold_sentence[0][i].tree in commas]

            gold_trees = [gold_sentence[0][x].tree for x in gold_sentence[0] if x not in comma_positions]
            #print gold_trees

            gold_heads = [gold_sentence[0][x].parent_id for x in gold_sentence[0] if x not in comma_positions]

            if test_sentence == "NP":
                #print "No prediction"
                total_no_parse += 1    
                total_count += 1
                length_sum += len(gold_heads)
                error_length_sum += len(gold_heads)
            elif test_sentence == "TO":
                #print "Time out"
                total_timeout += 1    
                total_count += 1
                length_sum += len(gold_heads)
                error_length_sum += len(gold_heads)
            else: 

                test_trees = [test_sentence[i].tree for i in range(len(test_sentence)) if i not in comma_positions]
                #print test_trees
                #assert gold_trees == test_trees
                test_heads = [test_sentence[i].parent_pos for i in range(len(test_sentence)) if i not in comma_positions]   
                correct, length, acc =  evaluate_heads(gold_heads, test_heads)
                total_count+=1
                length_sum += length
                correct_sum += correct
                accuracy_sum += acc
        print "Total sentence:", total_count
        print "No parse found:", total_no_parse
        print "Timeout:", total_timeout 
        print "macro AVG (all sentences):", accuracy_sum / total_count
        print "micro AVG (all sentences):", correct_sum / float(length_sum)
        print "macro AVG (parsed sentences):", accuracy_sum / (total_count - total_no_parse - total_timeout)
        print "micro AVG (parsed sentences):", correct_sum / float(length_sum - error_length_sum)
