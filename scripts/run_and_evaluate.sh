JAVA_HOME=/local/users/daniel/jdk1.8.0_51
JAVA=$JAVA_HOME/bin/java
PYTHON=python2.7
GBDP_DIR=/local/users/daniel/java-gbdp
EXPERIMENT_OUTPUT_DIR=$GBDP_DIR/experiments
mkdir -p $EXPERIMENT_OUTPUT_DIR

GRAMMAR=$GBDP_DIR/data/d6.clean2.nomissnonull.postns0.trees0.smooth0.001.cfg

INPUT=$GBDP_DIR/data/d6.clean2.00.stags.gold 
OUTPUT=$EXPERIMENT_OUTPUT_DIR/d6.clean2.00.predict.goldstags.trees0.beamn$BEAMWIDTH.deps

GOLD=$GBDP_DIR/data/d6.clean2.00.nomissnonull.deps


BEAMWIDTH=$1

$JAVA -Xmx8g -cp ~/.m2/repository/com/beust/jcommander/1.48/jcommander-1.48.jar:$GBDP_DIR/target/classes edu.columbia.nlp.gbdp.Main --beamn $BEAMWIDTH -gr $GRAMMAR $INPUT  > $OUTPUT 
$PYTHON scripts/evaluate_deps.py $GOLD $OUTPUT  > $OUTPUT.results.txt
