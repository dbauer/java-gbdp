import sys

def compare_tags(gold, predict):

    correct = 0
    assert len(gold)==len(predict)
    for i in range(len(gold)):
        if gold[i]==predict[i]:
            correct+=1
    
    return correct, correct/float(len(gold))
        

with open(sys.argv[1]) as gold_file, open(sys.argv[2]) as predict_file: 
    
    total_correct = 0
    total_length = 0
    acc_sum = 0

    i=0
    for gold_line in gold_file: 
        predict_line = predict_file.next()
        predict_tags = predict_line.strip().split()
        gold_tags = gold_line.strip().split()
        print gold_tags
        print predict_tags
        correct, acc =  compare_tags(gold_tags, predict_tags)
        total_correct += correct
        acc_sum += acc
        i+=1
        total_length += len(gold_tags)
        print correct, "/", len(gold_tags), "=", acc 
        
    print "==="
    print "Macro avg:", acc_sum / float(i)
    print "Micro avg:", total_correct / float(total_length)

