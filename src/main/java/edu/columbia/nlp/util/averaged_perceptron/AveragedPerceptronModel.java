package edu.columbia.nlp.util.averaged_perceptron;

import java.io.Serializable;
import java.util.Map;

/**
 * A wrapper class that contains feature definitions and the actual model (a feature vector for
 * each class).
 */
public class AveragedPerceptronModel<T extends DataInstance> implements Serializable {

  private static final long serialVersionUID = -6048136540606983137L;

  /**
   * @serial
   */
  private Map<String, SparseVector> weights;
  /**
   * @serial
   */
  private FeatureSet<T> features;

  public AveragedPerceptronModel(FeatureSet<T> features, Map<String, SparseVector> weights) {
    this.features = features;
    this.weights = weights;
  }

  public Map<String, SparseVector> getWeights() {
    return weights;
  }

  public FeatureSet<T> getFeatures() {
    return features;
  }

}
