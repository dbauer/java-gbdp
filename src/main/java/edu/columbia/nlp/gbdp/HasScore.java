package edu.columbia.nlp.gbdp;

import edu.columbia.nlp.util.Log;

/**
 * Created by daniel on 6/29/16.
 */
public abstract class HasScore implements Comparable<HasScore> {

  double score;
  double heuristic = 0.0;

  public double getScore() {
    return score;
  };

  public double getHeuristic() {
    return heuristic;
  }

  public void setScore(double score) {
    this.score = score;
  };

  public void setHeuristic(double heuristic) {
    this.heuristic = heuristic;
  }

  public double getSum() {
    return Log.logAdd(score,heuristic);
  }

  public int compareTo(HasScore other) {

      double thisf = getSum();
      double otherf = other.getSum();

      if (thisf == otherf) return 0;
      return thisf > otherf ? 1 : -1;
  }

}
