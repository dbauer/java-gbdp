package edu.columbia.nlp.gbdp.forest;
import edu.columbia.nlp.gbdp.grammar.Rule;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by daniel on 6/28/16.
 */
public class Hyperedge implements Comparable<Hyperedge>{

  public Rule rule;
  private ArrayList<List<Node>> children;

  public Hyperedge(Rule r) {
    this.rule = r;
    children = new ArrayList<>(r.rhs.size());
    for (int i=0; i< r.rhs.size(); i++)
      children.add(new ArrayList<Node>());
  }

  public void addChild(int position, Node child) {
    List<Node> list_for_position = children.get(position);
    list_for_position.add(0,child); // maybe make children a linkedlist?
  }

  public void addChildEnd(int position, Node child) {
    List<Node> list_for_position = children.get(position);
    list_for_position.add(child);
  }

  public ArrayList<List<Node>> getChildren() {
    return children;
  }

  public List<Node> getFlatChildren() {
    ArrayList<Node> result = new ArrayList<>();
    for (List<Node> position : children)
      for (Node child : position)
        result.add(child);
    return result;
  }


  public double score;

  @Override
  public int compareTo(Hyperedge o) {

    if (score > o.score)
      return 1;
    else if(o.score > score)
      return -1;
    return 0;

  }
}
