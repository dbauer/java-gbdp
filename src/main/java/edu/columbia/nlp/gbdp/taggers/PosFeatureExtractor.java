package edu.columbia.nlp.gbdp.taggers;

import edu.columbia.nlp.util.averaged_perceptron.*;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Stream;

/**
 * Created by daniel on 7/20/16.
 */
public class PosFeatureExtractor {

  protected static FeatureSet<NgramDataInstance> makeFeatureSet() {
    FeatureSet<NgramDataInstance> features = new FeatureSet<>();

    OneHotFeature<NgramDataInstance, String> thissuffix2 = new OneHotFeature<NgramDataInstance, String>(x->x.getSuffix(2,2), 100*100);
    features.addVectorFeature("thissuffix", thissuffix2);

    OneHotFeature<NgramDataInstance, String> thissuffix3 = new OneHotFeature<>(x->x.getSuffix(2,3), 100*100*100);
    features.addVectorFeature("thissufix3", thissuffix3);

    OneHotFeature<NgramDataInstance, String> thissuffix1 = new OneHotFeature<>(x->x.getSuffix(2,1), 100);
    features.addVectorFeature("thissufix1", thissuffix1);

    OneHotFeature<NgramDataInstance, String> psuffix1 = thissuffix1.spawn(x->x.getSuffix(1,1));
    features.addVectorFeature("psufix1", psuffix1);

    OneHotFeature<NgramDataInstance, String> ppsuffix1 = thissuffix1.spawn(x->x.getSuffix(0,1));
    features.addVectorFeature("ppsufix1", ppsuffix1);

    OneHotFeature<NgramDataInstance, String> psuffix2 = thissuffix2.spawn(x->x.getSuffix(1,2));
    features.addVectorFeature("psuffix2", psuffix2);

    OneHotFeature<NgramDataInstance, String> ppsuffix2 = thissuffix2.spawn(x->x.getSuffix(0,2));
    features.addVectorFeature("ppsuffix2", ppsuffix2);

    OneHotFeature<NgramDataInstance, String> psuffix3 = thissuffix3.spawn(x->x.getSuffix(1,3));
    features.addVectorFeature("psuffix3", psuffix3);

    OneHotFeature<NgramDataInstance, String> ppsuffix3 = thissuffix3.spawn(x->x.getSuffix(0,3));
    features.addVectorFeature("ppsufix3", ppsuffix3);

    OneHotFeature<NgramDataInstance, String> thisprefix1 = thissuffix3.spawn(x->x.getPrefix(2,1));
    features.addVectorFeature("thisprefix1", thisprefix1);

    OneHotFeature<NgramDataInstance, String> ppos = new OneHotFeature<>(x->x.getPos(1), 120);
    features.addVectorFeature("ppos", ppos);

    OneHotFeature<NgramDataInstance, String> pppos = ppos.spawn(x->x.getPos(0));
    features.addVectorFeature("pppos", pppos);

    OneHotFeature<NgramDataInstance, String> prevpos = new OneHotFeature<>(x->String.format("%s_%s",x.getPos(0),x.getPos(1)), 1600);
    features.addVectorFeature("prevpos",prevpos);

    OneHotFeature<NgramDataInstance, String> thisword = new OneHotFeature<>(x->x.getWord(2), 100000);
    features.addVectorFeature("thisword", thisword);

    OneHotFeature<NgramDataInstance, String> pword = thisword.spawn(x->x.getWord(1));
    features.addVectorFeature("pword", pword);

    OneHotFeature<NgramDataInstance, String> ptag_thisword = new OneHotFeature<>(x->String.format("%s_%s", x.getPos(1), x.getWord(2)), 1000000);
    features.addVectorFeature("ptag_thisword", ptag_thisword);

    Predicate<NgramDataInstance> containsDash = (x -> x.containsDash(2));
    features.addBinaryFeature("containsDash", containsDash);

    Predicate<NgramDataInstance> isNumber = (x -> x.isNumber(2));
    features.addBinaryFeature("thisIsNumber", isNumber);

    OneHotFeature<NgramDataInstance, String> nextword = thisword.spawn(x->x.getWord(3));
    features.addVectorFeature("nextword", nextword);

    OneHotFeature<NgramDataInstance, String> nextsuffix2 = thissuffix2.spawn(x->x.getSuffix(3,2));
    features.addVectorFeature("nextsuffix2", nextsuffix2);

    OneHotFeature<NgramDataInstance, String> nextsuffix3 = thissuffix3.spawn(x->x.getSuffix(3,3));
    features.addVectorFeature("nextsuffix3", nextsuffix3);

    OneHotFeature<NgramDataInstance, String> nextsuffix1 = thissuffix1.spawn(x->x.getSuffix(3,1));
    features.addVectorFeature("nextsuffix1", nextsuffix1);

    return features;
  }

  public static List<NgramDataInstance> readTrainingData(String inputFile) throws IOException {
    Stream<String> stream = Files.lines(Paths.get(inputFile));
    Stream<List<Token>> sentence_stream = stream.map(x->Token.parseLine(x));
    List<NgramDataInstance> instances = new ArrayList<>();

    sentence_stream.forEach(new Consumer<List<Token>>() {
      public void accept(List<Token> sentence) {
        for (Token[] ngram : Token.getNgrams(4, sentence)) {
          if (ngram[2]!=null) {
            NgramDataInstance instance = new NgramDataInstance(ngram[2].pos, ngram);
            instances.add(instance);
          }
        }
      }
    });
    return instances;
  }

  public static void training_iteration(AveragedPerceptron<NgramDataInstance> ap, List<NgramDataInstance> data, FeatureSet<NgramDataInstance> features) {
    int count = 0;
    for (NgramDataInstance instance : data) {
      if (count % 1000 == 0)
        System.out.println(count);
      ap.training_step(new ClassificationInstance(instance.getLabel(), features.apply(instance), instance.getDescription()));
      count ++;
    }
  }

  public static void main(String[] argv) throws IOException {
    PosFeatureExtractor extractor = new PosFeatureExtractor();
    List<NgramDataInstance> data = readTrainingData(argv[0]);
    System.err.println("Extracting features...");
    System.out.println(data.size());
    FeatureSet<NgramDataInstance> features = PosFeatureExtractor.makeFeatureSet();
    System.out.println("Done");

    //List<ClassificationInstance> trainingInstances = features.getClassificationInstances(data);

    System.out.println("Training...");
    AveragedPerceptron<NgramDataInstance> ap = new AveragedPerceptron<>();
    //training_iteration(ap, data, features);

    Map<String, SparseVector> values = ap.train(data,features,10);

    System.err.println("Evaluating...");
    List<NgramDataInstance> data2 = readTrainingData(argv[1]);
    List<ClassificationInstance> evalInstances = features.getClassificationInstances(data2);
    ap.evaluate_data(evalInstances);

    FileOutputStream fos = new FileOutputStream("postagger_model.dat");
    ObjectOutputStream oos = new ObjectOutputStream(fos);

    AveragedPerceptronModel<NgramDataInstance> model = new AveragedPerceptronModel<>(features, values);
    oos.writeObject(model);
    oos.close();
    System.out.println("saved model to postagger_mode.dat");
  }


}