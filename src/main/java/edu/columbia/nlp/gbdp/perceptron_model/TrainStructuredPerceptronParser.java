package edu.columbia.nlp.gbdp.perceptron_model;

import edu.columbia.nlp.gbdp.SentenceWithGold;
import edu.columbia.nlp.gbdp.parser.PcfgParser;
import edu.columbia.nlp.gbdp.parser.StructuredPerceptronParser;
import edu.columbia.nlp.gbdp.forest.Node;
import edu.columbia.nlp.gbdp.grammar.Grammar;
import edu.columbia.nlp.gbdp.taggers.Supertagger;
import edu.columbia.nlp.gbdp.taggers.Token;
import edu.columbia.nlp.util.averaged_perceptron.AveragedPerceptronModel;

import java.io.*;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;
import java.util.stream.Collectors;

/**
 * Created by bauer on 8/25/16.
 */
public class TrainStructuredPerceptronParser {


    public static List<SentenceWithGold> loadTrainingData(Grammar grammar, InputStream is) throws IOException {
        LinkedList<SentenceWithGold> result = new LinkedList<>();

        List<Node> derivations = Node.loadDerivations(grammar, is);

        return derivations.stream()
                    .map(derivation -> derivation!=null ? new SentenceWithGold(derivation.getDerivedString(), derivation) : new SentenceWithGold(null,derivation))
                    .collect(Collectors.toList());
    }

    public static List<List<Token[]>> loadStaggedInput(InputStream is) throws IOException{

      BufferedReader br = new BufferedReader(new InputStreamReader(is));
      List<List<Token[]>> result = new LinkedList<>();

      String line;
      while ((line=br.readLine())!=null) {
        List<Token[]> stags = Supertagger.parseNbestSupertaggedLine(line,1);
        result.add(stags);
      }

      return result;
    }

    public static void mergeInput(List<SentenceWithGold> derivations, List<List<Token[]>> stags) {

      Iterator<List<Token[]>> stagIter = stags.iterator();
      for (SentenceWithGold s : derivations) {
        if (s.derivation!=null)
          s.setSentence(stagIter.next());
      }

    }

    public static void main(String[] args) throws FileNotFoundException, IOException {

        System.err.print("Reading grammar...");
        Grammar grammar = Grammar.readGrammar(new FileInputStream(args[0]));
        System.err.print("done.\n");

        System.err.print("Reading input stags...");
        //List<List<Token[]>> staggedInput= loadStaggedInput(new FileInputStream(args[2]));
        List<List<Token[]>> staggedInput= Token.readInputWithTags(new FileInputStream(args[2])); //PcfgParser.readStagedInput(new FileInputStream(args[2]));

        System.err.print("Reading training derivations...");
        System.err.print("done.\n");
        List<SentenceWithGold> trainingData = loadTrainingData(grammar, new FileInputStream(args[1]));

        if (staggedInput.size() != trainingData.size()) {
          System.err.println("Supertagged inputs and gold derivations differ in length.");
          System.exit(1);
        }

        mergeInput(trainingData, staggedInput);
        trainingData = trainingData.stream().filter(s -> s.derivation!=null).collect(Collectors.toList());

        Properties config = new Properties();
        config.setProperty("grammarFile", args[0]);

        //if (commands.finishingAgendaBeamN != null) {
        //    config.setProperty("finishingAgendaBeamN", Integer.toString(commands.finishingAgendaBeamN));
        //    System.err.println("Use finishing agenda beam search with width "+config.getProperty("finishingAgendaBeamN"));
      //}

      StructuredPerceptronParser parser = new StructuredPerceptronParser(grammar, config);
      AveragedPerceptronModel<TraversalWithContext> model = parser.train(trainingData, 10);

      FileOutputStream fos = new FileOutputStream("parser_model.dat");
      ObjectOutputStream oos = new ObjectOutputStream(fos);
      oos.writeObject(model);
      oos.close();
      System.out.println("Structured perceptron model saved to parser_model.dat");


    }

}
