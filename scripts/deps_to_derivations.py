#!/usr/bin/python2.7

import sys
from collections import defaultdict

from deps_tools import DepsFile
import traceback

class Rule():
 
    def __init__(self, rule_id, lhs, rhs, score, anchor = None, anchor_index  = 0):
        self.rule_id = rule_id
        self.lhs = lhs
        self.rhs = rhs
        self.score = score
        self.anchor = anchor
        self.anchor_index = anchor_index


    def __repr__(self): 
        return "%i: %s -> %s ; %f" % (self.rule_id, self.lhs, " ".join(self.rhs), self.score)

def read_grammar(grammar_file):
    grammar = defaultdict(list)

    i = 0
    for line in grammar_file: 
        lhs, remain = line.strip().split("->")
        remain, score = remain.rsplit(";")
        score = float(score)
        tokens = remain.strip().split(" ")
        rhs = []
        anchor = None
        anchor_index = -1
        ind = 0
        for t in tokens:         
          
          if t != '':
            if t.endswith("$*") or t.endswith("$"):
                rhs.append(t)
            else: 
                if anchor:
                    if t != "tCO":   
                        if anchor != "tCO": 
                            print "Warning, duplicate anchor ",t,"in rule ", i, line.strip()
                        else:
                            anchor = t
                            anchor_index = ind
                else:
                    anchor = t
                    anchor_index = ind
                rhs.append(t)
          ind += 1  
        rule = Rule(i, lhs, rhs, score, anchor, anchor_index)
        grammar[anchor].append(rule)
        i += 1
    return grammar
        


class Converter():
    
    def __init__(self, grammar, deps):
        self.grammar = grammar
        self.deps = deps
        self.compute_children()

    def compute_children(self):
        self.children = defaultdict(list)
        deps = self.deps
        for x in deps: 
            if deps[x].parent_id != x:
                    self.children[deps[x].parent_id].append(x)
           
    def convert_sentence(self):
        deps = self.deps
        roots = [x for x in deps if deps[x].parent_id == x]
        return self.convert_sentence_rec(roots[0])
    
    def convert_sentence_rec(self, node):
        deps = self.deps
        if self.children[node] == []:
            result =[] 
            rules = grammar[deps[node].tree]
            for r in rules: 
               result.append((r,)) 

        subtress = []

        childlist = sorted([c for c in self.children[node] if deps[c].tree != "tCO" and self.children[node]!=[]])
        subtrees = [self.convert_sentence_rec(c) for c in childlist] 
        rules = grammar[deps[node].tree]
        if len(rules) > 0:
            if len(rules) > 1:
                sys.stderr.write("multiple rules for tree "+ deps[node].tree)
                sys.stderr.write(str(rules))
                sys.stderr.write("\n")
            r = rules[0]

            index_to_subtrees = defaultdict(list) 
            current_pos = 0
            #print node, r
            for i in range(len(childlist)):
               child = childlist[i]
               subtree = subtrees[i]
               if subtree is None: 
                   raise RuntimeError('no rule for tree '+deps[child].tree)
               nt = subtree[0].lhs
               #print i,child,  nt, deps[child].tree 
 
               right_of_anchor = child > node

               while current_pos < len(r.rhs) and nt.strip() != r.rhs[current_pos].replace("$","").replace("*","").strip(): #or \ (right_of_anchor and current_pos < r.anchor_index)):
                   current_pos += 1
              
               if current_pos >= len(r.rhs):
                   sys.stderr.write("No rule for this split.%s %s\n "% (r, nt))
                   raise RuntimeError("NO rule for split")
 
               index_to_subtrees[current_pos].append(subtree)
 
               if not r.rhs[current_pos].endswith("*"):
                   current_pos += 1

            return (r, index_to_subtrees)
        return  None 

    def to_string_list(self, tree):
            parent, children = tree
    
            childstrs = []
            if children:

                for i in range(len(parent.rhs)):
                    if i in children:
                        for c in children[i]:
                            childstrs.append("%s%i(%s)" % (parent.rhs[i], i, self.to_string_list(c)))
            result = "%s %s" % (parent.rule_id, " ".join(childstrs))
            return result

with open(sys.argv[1],'r') as grammar_f, open(sys.argv[2], 'r') as deps_f:

    grammar = read_grammar(grammar_f)

    sys.stderr.write("Read grammar.\n")

    corpus = DepsFile(deps_f)
    for sentences in corpus.sentence_iterator():
        sentence = sentences[0]

        conv = Converter(grammar, sentence)
        try:
            tree = conv.convert_sentence()
            print "(%s)" % conv.to_string_list(tree)
        except RuntimeError,e:
            pass
        except TypeError, e: 
            traceback.print_exc()            
            pass
    sys.stderr.write("done.\n")
