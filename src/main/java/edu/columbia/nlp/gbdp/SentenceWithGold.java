package edu.columbia.nlp.gbdp;

import edu.columbia.nlp.gbdp.forest.Node;
import edu.columbia.nlp.gbdp.taggers.Token;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by bauer on 8/25/16.
 */
public class SentenceWithGold {

    public List<Token[]> sentence;
    public Node derivation;
    public int stagbeam;

    public void setSentence(List<Token[]> sentence) {
        this.sentence = sentence;
    }

    public SentenceWithGold(List<String> sentence, Node derivation) {

        this.derivation = derivation;
        this.sentence = new ArrayList<>();
        if (sentence != null) {
            for (String tok : sentence) {
                Token[] toks = new Token[1];
                toks[0] = new Token(null, null, tok, 0.0);
                this.sentence.add(toks);
            }
        }
    }

    public SentenceWithGold(List<Token[]> sentence, Node derivation, int stagbeam) {
        this.sentence = sentence;
        this.derivation = derivation;
        this.stagbeam = stagbeam;
    }

}
