import sys,re

rule_re = re.compile("(?P<lhs>[^=]+)\s*=\s+(?P<rhs>[^;]*)\s*;\s+(?P<weight>[0-9\.\-e]+)")
token_re = re.compile("\s*\<(?P<token>[^\>]+)\>\s*")

def convert_token(token):
    match = token_re.match(token)
    if match: 
        tok = match.group('token')
        tok = tok.replace("$$","_dollardollar")
        tok = tok.replace("$","_dollar")
        if tok.endswith('*0'):
            tok = tok[:-2]
            tok = "%s$*" % tok
        else: 
            tok = "%s$" % tok
        return tok 
    else:
        return token

def convert_line(line):
    match = rule_re.match(line) 
    lhs = convert_token(match.group('lhs'))
    if lhs.endswith('$*'): 
        lhs = lhs[:-2]
    if lhs.endswith('$'): 
        lhs = lhs[:-1]
    rhs_tokens = match.group('rhs').strip().split(' ');
 
    tokens = [convert_token(token.strip()) for token in rhs_tokens if token.strip()]
    # Remove recursive adjunction. The parser handles this directly.
    if tokens and (tokens[-1][-1]=="*") and (tokens[-1][:-2]==lhs):
        tokens = tokens[:-1] # Remove recursive adjunction 
    
    return "%s -> %s ; %s" % (lhs, " ".join(tokens), match.group("weight"))   

def convert_file(input_f, output_f):
    for raw_line in input_f:
        line = raw_line.strip()
        if line and not line.startswith('*'):
            output_f.write(convert_line(line));
            output_f.write('\n')

if __name__ == '__main__': 

    if len(sys.argv) != 2:
        print("usage: python convert_mica_grammar.py [mica grammar file] > [output file]")
        sys.exit(1)

    with open(sys.argv[1],'r') as input_f:
        convert_file(input_f, sys.stdout)

